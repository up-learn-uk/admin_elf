[
  tools: [
    {:compiler, env: %{"MIX_ENV" => "test"}},
    {:formatter, env: %{"MIX_ENV" => "test"}},
    {:ex_doc, env: %{"MIX_ENV" => "test"}},
    {:npm_test, command: ~w[npm test -- --watch=false]},

    # TODO: remove this as soon as the API is finalized and properly documented
    {:doctor, false}
  ]
]
