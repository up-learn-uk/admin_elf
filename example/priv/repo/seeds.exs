alias AdminElfExample.Cars.{Brand, Car}
alias AdminElfExample.Companies.Company
alias AdminElfExample.Repo

Repo.delete_all(Car)
Repo.delete_all(Brand)
Repo.delete_all(Company)

create_brand = fn name ->
  Repo.insert!(%Brand{name: name})
end

create_car = fn brand, fields ->
  Car
  |> struct!(fields)
  |> Map.put(:brand_id, brand.id)
  |> Repo.insert!()
end

create_company = fn name ->
  Repo.insert!(%Company{name: name})
end

shelby = create_brand.("Shelby")
ford = create_brand.("Ford")
honda = create_brand.("Honda")
nissan = create_brand.("Nissan")
tesla = create_brand.("Tesla")

create_car.(shelby,
  name: "Mustang",
  year: 1965,
  price: 4_000_000,
  quantity: 30,
  popularity: 72,
  release_date: ~D[1965-01-27]
)

create_car.(ford,
  name: "Mustang",
  year: 1968,
  price: 60_000,
  quantity: 800,
  popularity: 65,
  release_date: ~D[1968-04-17]
)

create_car.(honda,
  name: "Civic",
  year: 1972,
  price: 20_000,
  quantity: 2000,
  popularity: 58,
  release_date: ~D[1972-04-28]
)

create_car.(nissan,
  name: "Rogue",
  year: 2007,
  price: 120_000,
  quantity: 120,
  popularity: 64,
  release_date: ~D[2007-10-12]
)

create_car.(tesla,
  name: "Model S",
  year: 2012,
  price: 90_000,
  quantity: 570,
  popularity: 70,
  release_date: ~D[2012-06-22]
)

create_company.("Car Expert")
