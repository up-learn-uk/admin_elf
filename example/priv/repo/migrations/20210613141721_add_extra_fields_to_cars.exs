defmodule AdminElfExample.Repo.Migrations.AddExtraFieldsToCars do
  use Ecto.Migration

  def change do
    create table("companies") do
      add :name, :string

      timestamps()
    end

    alter table("cars") do
      add :price, :decimal
      add :quantity, :integer
      add :popularity, :integer
      add :release_date, :date
      add :active, :boolean, default: true
      add :seller_id, references("companies")
    end
  end
end
