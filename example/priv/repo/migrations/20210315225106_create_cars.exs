defmodule AdminElfExample.Repo.Migrations.CreateCars do
  use Ecto.Migration

  def change do
    create table(:cars) do
      add :name, :string
      add :year, :integer
      add :brand_id, references(:brands, on_delete: :nothing)

      timestamps()
    end

    create index(:cars, [:brand_id])
  end
end
