<div class="bg-white shadow-md rounded-md p-8 mb-4">
  <h2 class="flex items-center gap-3 text-4xl">
    <svg fill="none" height="32" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" width="32">
      <use xlink:href="/admin-elf/icons/feather-sprite.svg#file"></use>
    </svg>
    Reports
  </h2>
</div>

<div class="flex-grow bg-white shadow-md rounded-md p-8">
   <h3 class="mb-4 text-xl flex-grow text-center">This is a custom page that uses liveview to update its own form fields</h3>
  <br/>
  <p>
    Everything rendered here is on the
    <a href="https://gitlab.com/up-learn-uk/admin_elf/-/blob/master/example/lib/admin_elf_example_web/live/report_live.html.heex">
      <b>lib/admin_elf_example_web/live/report_live.html.heex</b>
    </a>
    file
  </p>

  <.form :let={f} for={%{}} as={:report}>
    <div class="block mb-4">
      <label>Brand</label>
      <%= select f, "brand", @brands_options,
        "phx-change": "select_brand",
        prompt: "...",
        class: "mt-1 block w-full border rounded"
      %>
    </div>

    <div class="block mb-4">
      <label>Car</label>
      <%= select f, "car", @cars_options,
        "phx-change": "select_car",
        disabled: @cars_options == [],
        prompt: "...",
        class: "mt-1 block w-full border rounded"
      %>
    </div>

    <%= if @selected_car_info do %>
    <ul>
      <%= for {key, value} <- @selected_car_info do %>
      <li><b><%= String.capitalize("#{key}") %>: </b> <%= value %></li>
      <% end %>
    </ul>
    <% end %>
  </.form>
</div>

<div class="flex-grow bg-white shadow-md rounded-md p-8 mt-4">
  <h3 class="mb-4 text-xl flex-grow">Creating a LiveView Custom Page</h3>

  If this is your first LiveView CustomPage you should follow these steps:
  <br/>
  <br/>

  <ul>
    <li>On your <b>config.exs</b> file, set the <b>:live_socket_path</b> option as done in <b>example/config.exs</b>.</li>
    <li>Create a container view. The view's <b>form_layout</b> should return a template.</li>
    <li>Create a container template. The template should return <b> @inner_content </b></li>
    <li>Create a content view. The view's <b>form_layout</b> should return a template. The view should have <b>handle_event</b> callbacks to support <b>phx-change</b> events</li>
    <li>Create a content template. The template should return the desired code. The template should fire <b>phx-change</b> events via form inputs</li>
  </ul>
</div>
