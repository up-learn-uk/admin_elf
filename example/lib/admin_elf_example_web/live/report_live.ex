defmodule AdminElfExampleWeb.ReportLive do
  use AdminElfExampleWeb, :live_view

  alias AdminElfExample.Cars

  @impl true
  def mount(_params, _session, socket) do
    brands_options = Map.new(Cars.list_brands(), &{&1.name, &1.id})

    socket =
      assign(socket, brands_options: brands_options, cars_options: [], selected_car_info: nil)

    {:ok, socket}
  end

  @impl true
  def handle_event("select_brand", %{"report" => %{"brand" => ""}}, socket) do
    {:noreply, assign(socket, cars_options: [])}
  end

  @impl true
  def handle_event("select_brand", %{"report" => %{"brand" => brand_id}}, socket) do
    cars_options =
      [brand_id: brand_id]
      |> Cars.list_cars()
      |> Map.new(&{&1.name, &1.id})

    {:noreply, assign(socket, cars_options: cars_options)}
  end

  @impl true
  def handle_event("select_car", %{"report" => %{"car" => car_id}}, socket) do
    car_info =
      car_id
      |> Cars.get_car()
      |> Map.take([:name, :year, :price, :quantity, :popularity, :release_date, :active])

    {:noreply, assign(socket, selected_car_info: car_info)}
  end
end
