defmodule AdminElfExampleWeb.SellerLive do
  use AdminElfExampleWeb, :live_view

  alias AdminElfExample.{Cars, Companies}

  @impl true
  def mount(_params, _session, socket) do
    cars_options = Map.new(Cars.list_cars(), &{&1.name, &1.id})

    socket = assign(socket, cars_options: cars_options, selected_cars: [])

    {:ok, socket}
  end

  @impl true
  def handle_event("select_car", %{"seller" => %{"car" => car_id}}, socket) do
    selected_cars = Enum.uniq([Cars.get_car(car_id) | socket.assigns.selected_cars])

    {:noreply, assign(socket, selected_cars: selected_cars)}
  end

  @impl true
  def handle_event("save_seller", %{"name" => name}, socket) do
    with {:ok, seller} <- Companies.create_company(%{name: name}),
         :ok <- link_cars_to_seller(socket.assigns.selected_cars, seller) do
      {:noreply, assign(socket, selected_cars: [])}
    end
  end

  defp link_cars_to_seller(cars, seller) do
    Enum.each(cars, fn car ->
      {:ok, _} = Cars.update_car(car, %{seller_id: seller.id})
    end)

    :ok
  end
end
