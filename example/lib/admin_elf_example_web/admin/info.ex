defmodule AdminElfExampleWeb.Admin.Info do
  use AdminElf.CustomPage

  @impl true
  def title, do: "info"

  @impl true
  def icon, do: "info"

  @impl true
  def template(_conn, _params) do
    {
      AdminElfExampleWeb.Admin.InfoView,
      :index,
      info: [
        %{
          category: "Wiki",
          title: "Internal combustion engine",
          content: """
          An internal combustion engine (ICE) is a heat engine in which the combustion of a fuel occurs with an oxidizer (usually air)
          in a combustion chamber that is an integral part of the working fluid flow circuit. [...]
          """,
          link: "https://en.wikipedia.org/wiki/Internal_combustion_engine",
          accessed_at: ~N[2021-04-18 02:56:15]
        },
        %{
          category: "Wiki",
          title: "Car",
          content: """
          A car (or automobile) is a wheeled motor vehicle used for transportation. Most definitions of cars say that they run primarily on roads,
          seat one to eight people, have four wheels, and mainly transport people rather than goods. [...]
          """,
          link: "https://en.wikipedia.org/wiki/Car",
          accessed_at: ~N[2021-04-18 03:12:58]
        },
        %{
          category: "News",
          title: "A quick history of the British car industry",
          content:
            "In this quick history of the British car industry, economist Dan Coffey explains how a once thriving British-owned car manufacturing industry, largely disappeared.",
          link: "https://www.bbc.com/news/av/business-47330940",
          accessed_at: ~N[2021-04-18 03:15:18]
        }
      ]
    }
  end
end
