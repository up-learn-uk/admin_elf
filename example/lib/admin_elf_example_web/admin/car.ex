defmodule AdminElfExampleWeb.Admin.Car do
  use AdminElf.Resource

  alias AdminElfExample.Cars
  alias AdminElfExampleWeb.ChangesetView

  @impl true
  def icon, do: "truck"

  @impl true
  def name, do: "car"

  @impl true
  def item_display_name(car), do: car.name

  @impl true
  def allow_download(_conn), do: true

  @impl true
  def actions(_conn) do
    [
      action(:toggle_active, &toggle_active/3),
      action(:update_quantity, &update_quantity/3,
        inputs: [
          text_input(:quantity, label: "New quantity")
        ]
      )
    ]
  end

  @impl true
  def quick_actions(_conn) do
    [:detail, :edit, delete: [data: [confirm: "This cannot be undone. Are you sure?"]]]
  end

  @impl true
  def filters(_conn) do
    [
      select_input(:brand_id,
        label: "Brand",
        options: Enum.map(Cars.list_brands([], order: [asc: :name]), &{to_string(&1.id), &1.name})
      )
    ]
  end

  @impl true
  def index_table(_conn) do
    [
      text_column(:id, label: "ID", order: true),
      reference_column(:brand, ~w[cars brand]a),
      text_column(:name, order: true),
      text_column(:year),
      number_column(:price, format: :currency),
      number_column(:quantity),
      number_column(:popularity, format: :percentage),
      date_column(:inserted_at, label: "Created at"),
      boolean_column(:active)
    ]
  end

  @impl true
  def details_layout(_conn) do
    [
      panel("Details", [
        reference_field(:brand, ~w[cars brand]a),
        text_field(:name),
        text_field(:year),
        number_field(:price, format: :currency),
        number_field(:quantity),
        number_field(:popularity, format: :percentage),
        date_field(:release_date, label: "Release date"),
        reference_field(:seller, ~w[company]a),
        date_field(:inserted_at, label: "Created at", format: "{0D}/{0M}/{YYYY} {h24}:{m}:{s}")
      ]),
      panel("Status", [
        boolean_field(:active)
      ])
    ]
  end

  @impl true
  def form_layout(_conn) do
    [
      panel("Details", [
        select_input(:brand_id,
          label: "Brand",
          options:
            Enum.map(Cars.list_brands([], order: [asc: :name]), &{to_string(&1.id), &1.name})
        ),
        text_input(:name),
        number_input(:year),
        number_input(:price, format: :currency),
        number_input(:quantity),
        number_input(:popularity, format: :percentage),
        date_input(:release_date, label: "Release date")
      ]),
      panel("Status", [
        boolean_input(:active)
      ])
    ]
  end

  @impl true
  def list_resource(_conn, page, per_page, order, filter) do
    {Cars.list_cars(filter, order: order, page: page, per_page: per_page),
     Cars.count_cars(filter)}
  end

  @impl true
  def get_resource(_conn, item_id) do
    item_id
    |> Cars.get_car()
    |> case do
      nil -> {:error, :not_found}
      car -> {:ok, car}
    end
  end

  @impl true
  def create_resource(_conn, attrs) do
    attrs
    |> Cars.create_car()
    |> case do
      {:ok, car} -> {:ok, car}
      {:error, changeset} -> {:error, ChangesetView.translate_errors(changeset)}
    end
  end

  @impl true
  def update_resource(_conn, car, attrs) do
    car
    |> Cars.update_car(attrs)
    |> case do
      {:ok, car} -> {:ok, car}
      {:error, changeset} -> {:error, ChangesetView.translate_errors(changeset)}
    end
  end

  @impl true
  def delete_resource(_conn, car) do
    car
    |> Cars.delete_car()
    |> case do
      {:ok, car} -> {:ok, car}
      {:error, changeset} -> {:error, ChangesetView.translate_errors(changeset)}
    end
  end

  defp toggle_active(_conn, car, _inputs) do
    case Cars.update_car(car, %{active: not car.active}) do
      {:ok, car} ->
        new_status_text = if(car.active, do: "active", else: "inactive")

        {:ok, "The status was set to #{new_status_text}."}

      {:error, _car} ->
        {:error, "The status could not be updated."}
    end
  end

  defp update_quantity(_conn, car, inputs) do
    case Cars.update_car(car, inputs) do
      {:ok, car} -> {:ok, "The quantity was set to #{car.quantity}."}
      {:error, _car} -> {:error, "The quantity could not be updated."}
    end
  end
end
