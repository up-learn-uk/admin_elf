defmodule AdminElfExampleWeb.Admin.Report do
  use AdminElf.CustomPage

  @impl true
  def title, do: "reports"

  @impl true
  def icon, do: "file"

  @impl true
  def template(_conn, _params) do
    {AdminElfExampleWeb.ReportLive, :index, []}
  end
end
