defmodule AdminElfExampleWeb.Admin.Company do
  use AdminElf.Resource

  alias AdminElfExample.Companies

  @impl true
  def icon, do: "globe"

  @impl true
  def name, do: "company"

  @impl true
  def plural_name, do: "companies"

  @impl true
  def item_id(company), do: company.id

  @impl true
  def item_display_name(company), do: company.name

  @impl true
  def details_layout(_conn) do
    simple_layout([
      text_field(:name),
      date_field(:inserted_at, label: "Created at", format: "{0D}/{0M}/{YYYY} {h24}:{m}:{s}")
    ])
  end

  @impl true
  def get_resource(_conn, item_id) do
    item_id
    |> Companies.get_company()
    |> case do
      nil -> {:error, :not_found}
      company -> {:ok, company}
    end
  end
end
