defmodule AdminElfExampleWeb.Admin.Brand do
  use AdminElf.Resource

  alias AdminElfExample.Cars

  @impl true
  def icon, do: "globe"

  @impl true
  def name, do: "brand"

  @impl true
  def item_display_name(brand), do: brand.name

  @impl true
  def allow_creation(_conn), do: true

  @impl true
  def allow_download(_conn), do: true

  @impl true
  def quick_actions(_conn) do
    [
      :detail,
      :edit,
      external_link: [
        label: "Search on Google",
        fun: fn brand -> "https://www.google.com/search?q=#{brand.name}" end
      ]
    ]
  end

  @impl true
  def filters(_conn) do
    [
      text_input(:name__contains, label: "Search")
    ]
  end

  @impl true
  def index_table(_conn) do
    [
      text_column(:id, label: "ID"),
      text_column(:name, order: true),
      date_column(:inserted_at, label: "Created at", order: true)
    ]
  end

  @impl true
  def details_layout(_conn) do
    simple_layout([
      text_field(:name),
      date_field(:inserted_at, label: "Created at", format: "{0D}/{0M}/{YYYY} {h24}:{m}:{s}"),
      reference_field(:cars, ~w[cars car]a),
      link_field(:models,
        text: "List of models",
        link: fn brand -> "https://www.google.com/search?q=list+of+#{brand.name}+models" end
      )
    ])
  end

  @impl true
  def form_layout(_conn) do
    {AdminElfExampleWeb.BrandView, "form.html", []}
  end

  @impl true
  def list_resource(_conn, page, per_page, order, filter) do
    {
      Cars.list_brands(filter, order: order, page: page, per_page: per_page),
      Cars.count_brands(filter)
    }
  end

  @impl true
  def get_resource(_conn, item_id) do
    item_id
    |> Cars.get_brand()
    |> case do
      nil -> {:error, :not_found}
      brand -> {:ok, brand}
    end
  end
end
