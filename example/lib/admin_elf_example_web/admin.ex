defmodule AdminElfExampleWeb.Admin do
  @behaviour AdminElf.Admin

  alias AdminElfExampleWeb.Admin.{Brand, Car, Company, Info, Report, Seller}

  @impl true
  def title, do: "Example Admin"

  @impl true
  def features(_conn) do
    %{
      cars: %{
        car: Car,
        brand: Brand,
        seller: Seller,
        info: Info,
        report: Report
      },
      company: Company
    }
  end

  @impl true
  def menu(_conn) do
    import AdminElf.MenuHelpers

    [
      feature(~w[cars car]a),
      feature(~w[cars brand]a),
      feature(~w[cars seller]a),
      feature(~w[cars info]a),
      feature(~w[cars report]a)
    ]
  end
end
