defmodule AdminElfExampleWeb.CarController do
  use AdminElfExampleWeb, :controller

  alias AdminElfExample.Cars

  action_fallback AdminElfExampleWeb.FallbackController

  def index(conn, _params) do
    cars = Cars.list_cars()
    render(conn, "index.json", cars: cars)
  end

  def show(conn, %{"id" => id}) do
    car = Cars.get_car(id)
    render(conn, "show.json", car: car)
  end
end
