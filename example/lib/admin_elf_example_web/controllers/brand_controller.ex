defmodule AdminElfExampleWeb.BrandController do
  use AdminElfExampleWeb, :controller

  alias AdminElfExample.Cars
  alias AdminElfExample.Cars.Brand

  action_fallback AdminElfExampleWeb.FallbackController

  def index(conn, _params) do
    brands = Cars.list_brands()
    render(conn, "index.json", brands: brands)
  end

  def show(conn, %{"id" => id}) do
    brand = Cars.get_brand(id)
    render(conn, "show.json", brand: brand)
  end

  def create(conn, %{"name" => name}) do
    case Cars.create_brand(%{name: name}) do
      {:ok, %Brand{}} ->
        redirect(conn, to: "/admin/cars__brand")

      _ ->
        render(conn, "index.json", brands: Cars.list_brands())
    end
  end
end
