defmodule AdminElfExample.Repo do
  use Ecto.Repo,
    otp_app: :admin_elf_example,
    adapter: Ecto.Adapters.Postgres
end
