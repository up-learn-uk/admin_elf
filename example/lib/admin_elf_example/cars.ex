defmodule AdminElfExample.Cars do
  @moduledoc """
  The Cars context.
  """

  import Ecto.Query, warn: false
  alias AdminElfExample.Repo

  alias AdminElfExample.Cars.Brand

  @doc """
  Returns the list of brands.

  ## Examples

      iex> list_brands()
      [%Brand{}, ...]

  """
  def list_brands(filters \\ [], opts \\ []) do
    filters
    |> Brand.QueryBuilder.build_query(opts)
    |> Repo.all()
    |> Repo.preload(:cars)
  end

  def count_brands(filters \\ []) do
    filters
    |> Brand.QueryBuilder.build_query()
    |> Repo.aggregate(:count)
  end

  @doc """
  Gets a single brand.

  ## Examples

      iex> get_brand(123)
      %Brand{}

      iex> get_brand(456)
      nil

  """
  def get_brand(id) do
    Brand
    |> preload(:cars)
    |> Repo.get(id)
  end

  @doc """
  Gets a single brand.

  Raises `Ecto.NoResultsError` if the brand does not exist.

  ## Examples

      iex> get_brand!(123)
      %Brand{}

  """
  def get_brand!(id) do
    Brand
    |> preload(:cars)
    |> Repo.get!(id)
  end

  @doc """
  Creates a brand.

  ## Examples

      iex> create_brand(%{field: value})
      {:ok, %Brand{}}

      iex> create_brand(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_brand(attrs \\ %{}) do
    %Brand{}
    |> Brand.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a brand.

  ## Examples

      iex> update_brand(brand, %{field: new_value})
      {:ok, %Brand{}}

      iex> update_brand(brand, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_brand(%Brand{} = brand, attrs) do
    brand
    |> Brand.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a brand.

  ## Examples

      iex> delete_brand(brand)
      {:ok, %Brand{}}

      iex> delete_brand(brand)
      {:error, %Ecto.Changeset{}}

  """
  def delete_brand(%Brand{} = brand) do
    brand
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.no_assoc_constraint(:cars)
    |> Repo.delete()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking brand changes.

  ## Examples

      iex> change_brand(brand)
      %Ecto.Changeset{data: %Brand{}}

  """
  def change_brand(%Brand{} = brand, attrs \\ %{}) do
    Brand.changeset(brand, attrs)
  end

  alias AdminElfExample.Cars.Car

  @doc """
  Returns the list of cars.

  ## Examples

      iex> list_cars()
      [%Car{}, ...]

  """
  def list_cars(filters \\ [], opts \\ []) do
    filters
    |> Car.QueryBuilder.build_query(opts)
    |> Repo.all()
    |> Repo.preload([:brand, :seller])
  end

  def count_cars(filters \\ []) do
    filters
    |> Car.QueryBuilder.build_query()
    |> Repo.aggregate(:count)
  end

  @doc """
  Gets a single car.

  ## Examples

      iex> get_car(123)
      %Car{}

      iex> get_car(456)
      nil

  """
  def get_car(id) do
    Car
    |> preload([:brand, :seller])
    |> Repo.get(id)
  end

  @doc """
  Gets a single car.

  Raises `Ecto.NoResultsError` if the car does not exist.

  ## Examples

      iex> get_car!(123)
      %Car{}

  """
  def get_car!(id) do
    Car
    |> preload([:brand, :seller])
    |> Repo.get!(id)
  end

  @doc """
  Creates a car.

  ## Examples

      iex> create_car(%{field: value})
      {:ok, %Car{}}

      iex> create_car(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_car(attrs \\ %{}) do
    %Car{}
    |> Car.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a car.

  ## Examples

      iex> update_car(car, %{field: new_value})
      {:ok, %Car{}}

      iex> update_car(car, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_car(%Car{} = car, attrs) do
    car
    |> Car.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a car.

  ## Examples

      iex> delete_car(car)
      {:ok, %Car{}}

      iex> delete_car(car)
      {:error, %Ecto.Changeset{}}

  """
  def delete_car(%Car{} = car) do
    Repo.delete(car)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking car changes.

  ## Examples

      iex> change_car(car)
      %Ecto.Changeset{data: %Car{}}

  """
  def change_car(%Car{} = car, attrs \\ %{}) do
    Car.changeset(car, attrs)
  end
end
