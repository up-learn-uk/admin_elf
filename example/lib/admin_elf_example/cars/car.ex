defmodule AdminElfExample.Cars.Car do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cars" do
    field :name, :string
    field :year, :integer
    field :price, :decimal
    field :quantity, :integer
    field :popularity, :integer
    field :release_date, :date
    field :active, :boolean

    belongs_to :brand, AdminElfExample.Cars.Brand
    belongs_to :seller, AdminElfExample.Companies.Company

    timestamps()
  end

  @doc false
  def changeset(car, attrs) do
    car
    |> cast(attrs, [
      :brand_id,
      :name,
      :year,
      :price,
      :quantity,
      :popularity,
      :release_date,
      :seller_id,
      :active
    ])
    |> validate_required([:brand_id, :name, :year])
  end
end

defmodule AdminElfExample.Cars.Car.QueryBuilder do
  use QueryElf,
    schema: AdminElfExample.Cars.Car,
    searchable_fields: ~w[id brand_id name year active]a,
    sortable_fields: ~w[id name year inserted_at]a,
    plugins: [QueryElf.Plugins.OffsetPagination]
end
