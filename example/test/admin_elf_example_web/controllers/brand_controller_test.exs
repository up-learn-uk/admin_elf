defmodule AdminElfExampleWeb.BrandControllerTest do
  use AdminElfExampleWeb.ConnCase

  alias AdminElfExample.Cars

  @create_attrs %{
    name: "some name"
  }

  def fixture(:brand) do
    {:ok, brand} = Cars.create_brand(@create_attrs)
    brand
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup :create_brand

  describe "index" do
    test "lists all brands", %{conn: conn} do
      conn = conn |> authenticated() |> get(Routes.brand_path(conn, :index))
      assert [%{"id" => _, "name" => "some name"}] = json_response(conn, 200)["data"]
    end
  end

  defp create_brand(_) do
    brand = fixture(:brand)
    %{brand: brand}
  end
end
