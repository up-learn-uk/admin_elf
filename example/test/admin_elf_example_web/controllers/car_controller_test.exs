defmodule AdminElfExampleWeb.CarControllerTest do
  use AdminElfExampleWeb.ConnCase

  alias AdminElfExample.Cars

  @create_attrs %{
    name: "some name",
    year: 42
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup :create_car

  describe "index" do
    test "lists all cars", %{conn: conn} do
      conn = conn |> authenticated() |> get(Routes.car_path(conn, :index))
      assert [%{"id" => _, "name" => "some name"}] = json_response(conn, 200)["data"]
    end
  end

  defp create_car(_) do
    {:ok, brand} = Cars.create_brand(%{name: "brand name"})
    {:ok, car} = @create_attrs |> Map.put(:brand_id, brand.id) |> Cars.create_car()

    %{car: car}
  end
end
