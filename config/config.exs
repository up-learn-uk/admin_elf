import Config

config :phoenix, :json_library, Jason

# this configuration is only used during tests
if Mix.env() == :test do
  config :logger, level: :warn

  config :admin_elf, AdminElfWeb.TestEndpoint,
    secret_key_base: "FypDYxTyJeIvcTo6iyK0OCFqeTVCX6XTAO0dRJYmPu4jJ7/vgMqk9vDfaQ4NKe7X"
end
