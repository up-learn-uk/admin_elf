defmodule AdminElfWeb.CustomPageTest do
  use AdminElfWeb.ConnCase

  alias AdminElf.CustomPage.Router.Helpers, as: Routes
  alias AdminElfWeb.Helpers

  defmodule Admin.DateTimePageView do
    use AdminElfWeb, :view

    def render("index.html", %{datetime: datetime}) do
      "Your current datetime is " <> Calendar.strftime(datetime, "%d %B %Y %H:%M")
    end
  end

  defmodule DateTimePage do
    use AdminElf.CustomPage

    @impl true
    def title, do: "custom page test"

    @impl true
    def icon, do: "test"

    @impl true
    def template(_conn, _params) do
      {Admin.DateTimePageView, :index, %{datetime: NaiveDateTime.local_now()}}
    end
  end

  setup do
    %{conn: build_conn(features: %{datetime_page: DateTimePage})}
  end

  describe "index" do
    test "renders the custom page index", %{conn: conn} do
      conn = get(conn, Routes.custom_page_path(conn, :index, Helpers.encode_id([:datetime_page])))

      assert html_response(conn, 200) =~ "Your current datetime is"
    end
  end
end
