defmodule AdminElfWeb.PageControllerTest do
  use AdminElfWeb.ConnCase

  setup :setup_conn

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Nothing to show here."
  end
end
