defmodule AdminElfWeb.ResourceControllerTest do
  use AdminElfWeb.ConnCase
  import Mock

  alias AdminElf.Resource.Router.Helpers, as: Routes
  alias AdminElfWeb.Admin.TestCar, as: Car
  alias AdminElfWeb.Helpers

  def car_mock do
    {
      Car,
      [:passthrough],
      form_layout: fn _conn -> {AdminElfWeb.TestCarView, "form.html", []} end
    }
  end

  setup do
    %{conn: build_conn(features: %{car: Car})}
  end

  describe "list" do
    test "renders the resource list", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :list, Helpers.encode_id([:car])))

      assert_received {:list_resource_called, Car,
                       %{page: 1, per_page: 25, order: [], filter: %{}}}

      page = html_response(conn, 200)

      assert page =~ "Cars"
      assert page =~ "Id"
      assert page =~ "Name"

      assert page =~ "1"
      assert page =~ "Mustang"

      assert page =~ "2"
      assert page =~ "Shelby"

      assert page =~ "Showing 2 items."

      assert page =~ "Search by name"
    end

    test "passes only registered filters to the list_resource function", %{conn: conn} do
      get(
        conn,
        Routes.resource_path(conn, :list, Helpers.encode_id([:car]), filter: [id: 2, name: "mus"])
      )

      assert_received {:list_resource_called, Car, %{filter: %{name: "mus"}}}
    end

    test "passes page and per_page arguments to the list_resource function", %{conn: conn} do
      get(
        conn,
        Routes.resource_path(conn, :list, Helpers.encode_id([:car]), page: 3, per_page: 10)
      )

      assert_received {:list_resource_called, Car, %{page: 3, per_page: 10}}
    end

    test "passes order arguments to the list_resource function", %{conn: conn} do
      get(
        conn,
        Routes.resource_path(conn, :list, Helpers.encode_id([:car]),
          order: [field: :id, direction: :asc]
        )
      )

      assert_received {:list_resource_called, Car, %{order: [asc: :id]}}
    end
  end

  describe "download" do
    test "downloads the resource list", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :download, Helpers.encode_id([:car])))

      assert_received {:list_resource_called, Car, %{order: [], filter: %{}}}

      assert List.keyfind(conn.resp_headers, "content-disposition", 0) ==
               {"content-disposition", "attachment; filename=export-car.csv"}

      assert List.keyfind(conn.resp_headers, "content-type", 0) ==
               {"content-type", "text/csv; charset=utf-8"}
    end

    test "passes only registered filters to the list_resource function", %{conn: conn} do
      get(
        conn,
        Routes.resource_path(conn, :download, Helpers.encode_id([:car]),
          filter: [id: 2, name: "mus"]
        )
      )

      assert_received {:list_resource_called, Car, %{filter: %{name: "mus"}}}
    end

    test "passes page and per_page arguments to the list_resource function", %{
      conn: conn
    } do
      get(
        conn,
        Routes.resource_path(conn, :download, Helpers.encode_id([:car]), page: 3, per_page: 10)
      )

      assert_received {:list_resource_called, Car, %{page: 3, per_page: 10}}
    end

    test "passes order arguments to the list_resource function", %{conn: conn} do
      get(
        conn,
        Routes.resource_path(conn, :download, Helpers.encode_id([:car]),
          order: [field: :id, direction: :asc]
        )
      )

      assert_received {:list_resource_called, Car, %{order: [asc: :id]}}
    end
  end

  describe "detail" do
    test "renders the detail view", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :detail, Helpers.encode_id([:car]), 1))

      assert_received {:get_resource_called, Car, %{id: "1"}}

      page = html_response(conn, 200)

      assert page =~ "Cars"
      assert page =~ "1"
      assert page =~ "Name"
      assert page =~ "Mustang"
    end

    test "renders the registered custom actions", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :detail, Helpers.encode_id([:car]), 1))

      assert_received {:get_resource_called, Car, %{id: "1"}}

      page = html_response(conn, 200)

      assert page =~ "test_action"
      assert page =~ "Test action"
    end
  end

  describe "new" do
    test "renders the new view", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :new, Helpers.encode_id([:car])))

      page = html_response(conn, 200)

      assert page =~ "Cars"
      assert page =~ "Details"
      assert page =~ "Name"
      assert page =~ "Save"
    end

    test "renders a custom template view", %{conn: conn} do
      with_mocks([car_mock()]) do
        conn = get(conn, Routes.resource_path(conn, :new, Helpers.encode_id([:car])))

        page = html_response(conn, 200)
        assert page =~ "Cars Custom Template"
      end
    end
  end

  describe "edit" do
    test "renders the edit view", %{conn: conn} do
      conn = get(conn, Routes.resource_path(conn, :edit, Helpers.encode_id([:car]), 1))

      page = html_response(conn, 200)

      assert page =~ "Cars"
      assert page =~ "Details"
      assert page =~ "Name"
      assert page =~ "Mustang"
      assert page =~ "Save"
    end

    test "renders a custom template view", %{conn: conn} do
      with_mocks([car_mock()]) do
        conn = get(conn, Routes.resource_path(conn, :edit, Helpers.encode_id([:car]), 1))

        page = html_response(conn, 200)

        assert page =~ "Cars Custom Template"
      end
    end
  end

  describe "create" do
    test "creates a resource", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.resource_path(conn, :create, Helpers.encode_id([:car]), %{
            attrs: %{name: "Civic"}
          })
        )

      assert_received {:create_resource_called, Car, %{name: "Civic"}}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :notice) == "Resource created!"

      assert redirected_to(conn) == Routes.resource_path(conn, :list, Helpers.encode_id([:car]))
    end

    test "will fail to create a resource with invalid parameters", %{conn: conn} do
      conn =
        post(
          conn,
          Routes.resource_path(conn, :create, Helpers.encode_id([:car]), %{
            attrs: %{invalid_param: "Civic"}
          })
        )

      refute_received {:create_resource_called, Car, %{name: "Civic"}}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :error) == "An error occurred!"

      assert redirected_to(conn) == Routes.resource_path(conn, :list, Helpers.encode_id([:car]))
    end
  end

  describe "update" do
    test "updates a resource", %{conn: conn} do
      item_id = 10

      conn =
        patch(
          conn,
          Routes.resource_path(conn, :update, Helpers.encode_id([:car]), item_id, %{
            attrs: %{name: "Civic"}
          })
        )

      assert_received {:update_resource_called, Car,
                       {%{id: "10", name: "Mustang"}, %{name: "Civic"}}}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :notice) == "Resource updated!"

      assert redirected_to(conn) ==
               Routes.resource_path(conn, :edit, Helpers.encode_id([:car]), item_id)
    end

    test "will fail to update a resource with invalid parameters", %{conn: conn} do
      item_id = 10

      conn =
        patch(
          conn,
          Routes.resource_path(conn, :update, Helpers.encode_id([:car]), item_id, %{
            attrs: %{invalid_param: "Civic"}
          })
        )

      refute_received {:update_resource_called, Car,
                       {%{id: "10", name: "Mustang"}, %{name: "Civic"}}}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :error) == "An error occurred!"

      assert redirected_to(conn) ==
               Routes.resource_path(conn, :edit, Helpers.encode_id([:car]), item_id)
    end
  end

  describe "delete" do
    test "deletes a resource", %{conn: conn} do
      item_id = 10

      conn = delete(conn, Routes.resource_path(conn, :delete, Helpers.encode_id([:car]), item_id))

      assert_received {:delete_resource_called, Car, %{id: "10"}}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :notice) == "Resource deleted!"

      assert redirected_to(conn) == Routes.resource_path(conn, :list, Helpers.encode_id([:car]))
    end

    test "will fail to delete an unexisting resource", %{conn: conn} do
      item_id = 0

      conn = delete(conn, Routes.resource_path(conn, :delete, Helpers.encode_id([:car]), item_id))

      assert_received {:delete_resource_called, Car, nil}

      assert %{resource_id: _resource_id} = redirected_params(conn)
      assert get_flash(conn, :error) == "An error occurred!"

      assert redirected_to(conn) == Routes.resource_path(conn, :list, Helpers.encode_id([:car]))
    end
  end

  describe "custom_action" do
    test "runs the registered custom action function", %{conn: conn} do
      post(
        conn,
        Routes.resource_path(conn, :custom_action, Helpers.encode_id([:car]), 1, :test_action)
      )

      assert_received {:test_action_called, %{id: "1"}}
    end

    test "runs the registered custom action function with custom inputs", %{conn: conn} do
      action_name = :test_action_with_inputs

      post(
        conn,
        Routes.resource_path(conn, :custom_action, Helpers.encode_id([:car]), 1, action_name),
        %{inputs: %{name: "Mustang", year: 2000}}
      )

      assert_received {:test_action_with_inputs_called, %{id: "1"}, inputs_sent}
      assert inputs_sent == %{"name" => "Mustang"}
    end
  end
end
