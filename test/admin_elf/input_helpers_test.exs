defmodule AdminElf.InputHelpersTest do
  use ExUnit.Case
  alias AdminElf.InputHelpers

  describe "input_helpers" do
    test "should return a boolean input structure" do
      opts = [value: false]

      assert Enum.into(opts, %{
               type: :boolean,
               name: :active,
               label: "Active?"
             }) == InputHelpers.boolean_input(:active, opts)
    end

    test "should return a checkbox input structure" do
      opts = [
        options: [music: "Music", sports: "Sports", coding: "Coding"],
        value: [:coding]
      ]

      assert Enum.into(opts, %{
               type: :checkboxes,
               name: :interests,
               label: "Interests"
             }) == InputHelpers.checkboxes_input(:interests, opts)
    end

    test "should return a date input structure" do
      opts = [
        min: Date.utc_today(),
        max: Timex.shift(Date.utc_today(), days: 7),
        value: Date.utc_today()
      ]

      assert Enum.into(opts, %{
               type: :date,
               name: :renewal_date,
               label: "Renewal date"
             }) == InputHelpers.date_input(:renewal_date, opts)
    end

    test "should return a number input structure" do
      assert %{
               type: :number,
               name: :user_id,
               label: "User ID"
             } == InputHelpers.number_input(:user_id, label: "User ID")
    end

    test "should return a password input structure" do
      assert %{
               type: :password,
               name: :password,
               label: "Password"
             } == InputHelpers.password_input(:password)
    end

    test "should return a radio input structure" do
      opts = [options: [false: "No", true: "Yes"]]

      assert Enum.into(opts, %{
               type: :radio,
               name: :adult,
               label: "Adult",
               default_option: false
             }) == InputHelpers.radio_input(:adult, opts)
    end

    test "should return a select input structure" do
      opts = [options: [student: "Student", parent: "Parent", teacher: "Teacher"]]

      assert Enum.into(opts, %{
               type: :select,
               name: :role,
               label: "Role"
             }) == InputHelpers.select_input(:role, opts)
    end

    test "should return a multiselect input structure" do
      opts = [options: [student: "Student", parent: "Parent", teacher: "Teacher"]]

      assert Enum.into(opts, %{
               type: :multiselect,
               name: :roles,
               label: "Roles"
             }) == InputHelpers.multiselect_input(:roles, opts)
    end

    test "should return an email input structure" do
      assert %{type: :email, name: :email, label: "Email"} == InputHelpers.email_input(:email)
    end

    test "should return a text input structure" do
      assert %{type: :text, name: :name, label: "Name"} == InputHelpers.text_input(:name)
    end
  end
end
