defmodule AdminElfWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use AdminElfWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import AdminElfWeb.ConnCase

      # The default endpoint for testing
      @endpoint AdminElfWeb.TestEndpoint
    end
  end

  setup_all do
    import ExUnit.Callbacks

    start_supervised!(AdminElfWeb.TestEndpoint)

    :ok
  end

  def setup_conn(context) do
    admin_features = context[:admin_features] || %{}
    admin_menu = context[:admin_menu] || []

    %{conn: build_conn(features: admin_features, menu: admin_menu)}
  end

  def build_conn(opts \\ []) do
    features = opts[:features] || %{}
    menu = opts[:menu] || []

    admin_module = build_admin(features, menu)

    Phoenix.ConnTest.build_conn()
    |> Plug.Conn.put_private(AdminElf, %{admin: admin_module})
  end

  defp build_admin(features, menu) do
    quoted_features = Macro.escape(features)
    quoted_menu = Macro.escape(menu)

    admin_module_random_name_part =
      :crypto.strong_rand_bytes(32)
      |> Base.encode16()
      |> String.replace(~r/^(\d+)/, "")
      |> String.to_atom()

    admin_module_name = Module.concat(AdminElfWeb.TestAdmin, admin_module_random_name_part)

    Module.create(
      admin_module_name,
      quote do
        @behaviour AdminElf.Admin

        def title, do: "Test Admin"

        def menu(_conn), do: unquote(quoted_menu)

        def features(_conn), do: unquote(quoted_features)
      end,
      Macro.Env.location(__ENV__)
    )

    admin_module_name
  end
end
