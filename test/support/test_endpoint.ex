defmodule AdminElfWeb.TestEndpoint do
  use Phoenix.Endpoint, otp_app: :admin_elf

  @session_options [
    store: :cookie,
    key: "_admin_elf_key",
    signing_salt: "XgTPSzet"
  ]

  socket "/socket", AdminElfWeb.UserSocket,
    websocket: true,
    longpoll: false

  plug Plug.Static,
    at: "/",
    from: :admin_elf,
    gzip: false,
    only: ~w(css fonts images js icons robots.txt)

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options
  plug AdminElfWeb.Router
end
