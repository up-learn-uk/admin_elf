defmodule AdminElfWeb.Admin.TestCar do
  use AdminElf.Resource

  alias __MODULE__, as: Car

  defstruct [:id, :name]

  @impl true
  def icon, do: nil

  @impl true
  def name, do: "car"

  @impl true
  def plural_name, do: "cars"

  @impl true
  def item_id(item), do: item.id

  @impl true
  def item_display_name(item), do: item.name

  @impl true
  def actions(_conn) do
    [
      action(:test_action, fn _conn, item, _params ->
        send(self(), {:test_action_called, item})

        {:ok, nil}
      end),
      action(
        :test_action_with_inputs,
        fn _conn, item, params ->
          send(self(), {:test_action_with_inputs_called, item, params})

          {:ok, nil}
        end,
        inputs: [text_input(:name)]
      )
    ]
  end

  @impl true
  def filters(_conn) do
    [
      text_input(:name, label: "Search by name")
    ]
  end

  @impl true
  def index_table(_conn) do
    [
      text_column(:id),
      text_column(:name)
    ]
  end

  @impl true
  def details_layout(_conn) do
    simple_layout([
      text_field(:name)
    ])
  end

  @impl true
  def form_layout(_conn) do
    simple_layout([
      text_field(:name)
    ])
  end

  @impl true
  def list_resource(_conn, page, per_page, order, filter) do
    send(
      self(),
      {
        :list_resource_called,
        __MODULE__,
        %{page: page, per_page: per_page, order: order, filter: filter}
      }
    )

    {[%Car{id: 1, name: "Mustang"}, %Car{id: 2, name: "Shelby"}], 2}
  end

  @impl true
  def get_resource(_conn, "0") do
    send(self(), {:get_resource_called, __MODULE__, %{id: "0"}})

    {:ok, nil}
  end

  def get_resource(_conn, item_id) do
    send(self(), {:get_resource_called, __MODULE__, %{id: item_id}})

    {:ok, %Car{id: item_id, name: "Mustang"}}
  end

  @impl true
  def create_resource(_conn, %{"name" => name}) do
    send(self(), {:create_resource_called, __MODULE__, %{name: name}})

    {:ok, %Car{id: 3, name: name}}
  end

  def create_resource(_conn, attrs) do
    send(self(), {:create_resource_called, __MODULE__, attrs})

    {:error, :bad_request}
  end

  @impl true
  def update_resource(_conn, car, %{"name" => name} = _attrs) do
    send(self(), {:update_resource_called, __MODULE__, {car, %{name: name}}})

    {:ok, %Car{id: 3, name: name}}
  end

  def update_resource(_conn, car, attrs) do
    send(self(), {:update_resource_called, __MODULE__, {car, attrs}})

    {:error, :bad_request}
  end

  @impl true
  def delete_resource(_conn, nil) do
    send(self(), {:delete_resource_called, __MODULE__, nil})

    {:error, :bad_request}
  end

  def delete_resource(_conn, %{id: item_id} = _car) do
    send(self(), {:delete_resource_called, __MODULE__, %{id: item_id}})

    {:ok, %Car{id: item_id}}
  end
end
