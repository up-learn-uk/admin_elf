defmodule AdminElfWeb.TestCarView do
  use Phoenix.View,
    root: "test/support/templates",
    namespace: AdminElfWeb
end
