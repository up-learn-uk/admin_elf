defmodule AdminElf.MixProject do
  use Mix.Project

  @version "1.2.2"

  def project do
    [
      app: :admin_elf,
      version: @version,
      source_url: "https://gitlab.com/up-learn-uk/admin_elf",
      description: description(),
      package: package(),
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      docs: docs(),
      aliases: aliases(),
      deps: deps(),
      dialyzer: [
        plt_add_apps: [:ex_unit],
        plt_file: {:no_warn, "_build/plts/dialyzer.plt"}
      ],
      preferred_cli_env: [
        "test.watch": :test
      ],
      test_coverage: [summary: [threshold: 65]]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {AdminElf.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp description do
    "A handy tool for all your administrative needs."
  end

  defp package do
    [
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/up-learn-uk/admin_elf"}
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7"},
      {:phoenix_html, "~> 4.0"},
      {:phoenix_html_helpers, "~> 1.0"},
      {:phoenix_view, "~> 2.0"},
      {:phoenix_live_view, "~> 1.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:telemetry_metrics, "~> 1.0"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.18"},
      {:jason, "~> 1.2"},
      {:plug, "~> 1.16"},
      {:number, "~> 1.0"},
      {:timex, "~> 3.7"},
      {:mock, "~> 0.3"},
      {:mix_test_watch, "~> 1.0", only: :test, runtime: false},
      {:ex_check, "~> 0.14", only: [:dev, :test], runtime: false},
      {:credo, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:dialyxir, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:doctor, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:ex_doc, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:sobelow, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:csv, "~> 3.0"}
    ]
  end

  defp docs do
    [
      source_ref: "v#{@version}",
      main: "readme",
      extra_section: "GUIDES",
      extras: ["README.md"] ++ Path.wildcard("guides/*.md"),
      groups_for_extras: [
        guides: ~r(guides/[^/]+\.md)
      ]
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      tmwtd: ["test.watch --max-failures 1"],
      setup: ["deps.get", "cmd npm install --prefix assets"],
      "assets.deploy": ["cmd npm run deploy --prefix assets", "phx.digest"]
    ]
  end
end
