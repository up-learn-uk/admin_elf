defmodule AdminElf.CustomPage.Router do
  use Phoenix.Router

  scope "/", AdminElfWeb do
    get "/:custom_page_id", CustomPageController, :index
  end
end
