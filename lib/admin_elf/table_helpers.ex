defmodule AdminElf.TableHelpers do
  alias AdminElf.FieldHelpers

  @type column() :: %{
          type: FieldHelpers.field_type(),
          name: atom(),
          label: String.t(),
          value: (term() -> term()),
          link: (term() -> term()) | nil,
          referenced_resource_id: any(),
          order: atom()
        }

  @spec text_column(atom(), keyword()) :: column()
  def text_column(name, opts \\ []) do
    name
    |> FieldHelpers.text_field(opts)
    |> put_order(opts)
  end

  @spec date_column(atom(), keyword()) :: column()
  def date_column(name, opts \\ []) do
    name
    |> FieldHelpers.date_field(opts)
    |> put_order(opts)
  end

  @spec number_column(atom(), keyword()) :: column()
  def number_column(name, opts \\ []) do
    name
    |> FieldHelpers.number_field(opts)
    |> put_order(opts)
  end

  @spec boolean_column(atom(), keyword()) :: column()
  def boolean_column(name, opts \\ []) do
    name
    |> FieldHelpers.boolean_field(opts)
    |> put_order(opts)
  end

  @spec reference_column(atom(), [atom()], keyword()) :: column()
  def reference_column(name, referenced_resource_id, opts \\ []) do
    name
    |> FieldHelpers.reference_field(referenced_resource_id, opts)
    |> put_order(opts)
  end

  @spec link_column(atom(), keyword()) :: column()
  def link_column(name, opts \\ []) do
    name
    |> FieldHelpers.link_field(opts)
    |> Map.put(:order, false)
  end

  defp put_order(field, opts) do
    order =
      case Keyword.get(opts, :order, false) do
        true -> field.name
        value -> value
      end

    Map.put(field, :order, order)
  end
end
