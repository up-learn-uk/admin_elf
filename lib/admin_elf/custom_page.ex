defmodule AdminElf.CustomPage do
  @behaviour AdminElf.FeatureType

  import AdminElf.Feature, only: [fetch_feature!: 3]
  alias AdminElf.CustomPage.Router

  defmacro __using__(_) do
    quote do
      @behaviour AdminElf.CustomPage
      @behaviour AdminElf.Feature

      @impl AdminElf.Feature
      def type(), do: unquote(__MODULE__)
    end
  end

  @impl AdminElf.FeatureType
  def router, do: Router

  @impl AdminElf.FeatureType
  def default_path(conn, feature_id) do
    Router.Helpers.custom_page_path(conn, :index, feature_id)
  end

  @callback template(conn :: Plug.Conn.t(), params :: any) :: {module, atom, keyword}

  def get_template(conn, admin, id, params) do
    fetch_feature!(conn, admin, id).template(conn, params)
  end
end
