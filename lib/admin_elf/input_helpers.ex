defmodule AdminElf.InputHelpers do
  @type text_input() :: %{type: :text, name: atom(), label: String.t()}
  @type password_input() :: %{type: :password, name: atom(), label: String.t()}
  @type number_input() :: %{type: :number, name: atom(), label: String.t()}
  @type email_input() :: %{type: :email, name: atom(), label: String.t()}
  @type date_input() :: %{
          type: :date,
          name: atom(),
          label: String.t(),
          min: Date.t(),
          max: Date.t(),
          value: Date.t()
        }
  @type select_input() :: %{
          type: :select,
          name: atom(),
          label: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }
  @type multiselect_input() :: %{
          type: :multiselect,
          name: atom(),
          label: String.t(),
          options: [
            {value :: String.t(), label :: String.t(),
             meta :: %{
               # https://github.com/brianvoe/slim-select?tab=readme-ov-file#data-types
               text: String.t(),
               value: String.t(),
               html: String.t(),
               selected: boolean(),
               display: boolean(),
               disabled: boolean(),
               mandatory: boolean(),
               placeholder: boolean(),
               class: String.t(),
               style: String.t()
             }}
          ]
        }
  @type radio_input() :: %{
          type: :radio,
          name: atom(),
          label: String.t(),
          default_option: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }
  @type boolean_input() :: %{
          type: :boolean,
          name: atom(),
          label: String.t()
        }
  @type checkboxes_input() :: %{
          type: :checkboxes,
          name: atom(),
          label: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }

  @type input() ::
          boolean_input()
          | checkboxes_input()
          | date_input()
          | number_input()
          | password_input()
          | radio_input()
          | select_input()
          | multiselect_input()
          | text_input()
          | email_input()

  @spec boolean_input(atom(), keyword()) :: boolean_input()
  def boolean_input(name, opts \\ []) do
    input(name, :boolean, opts)
  end

  @spec checkboxes_input(atom(), keyword()) :: checkboxes_input()
  def checkboxes_input(name, opts \\ []) do
    input(name, :checkboxes, opts)
  end

  @spec date_input(atom(), keyword()) :: date_input()
  def date_input(name, opts \\ []) do
    input(name, :date, opts)
  end

  @spec number_input(atom(), keyword()) :: number_input()
  def number_input(name, opts \\ []) do
    input(name, :number, opts)
  end

  @spec password_input(atom(), keyword()) :: password_input()
  def password_input(name, opts \\ []) do
    input(name, :password, opts)
  end

  @spec radio_input(atom(), keyword()) :: radio_input()
  def radio_input(name, opts \\ []) do
    options = Keyword.fetch!(opts, :options)
    default_option = Keyword.get(opts, :default_option, options |> List.first() |> elem(0))

    input(name, :radio, Keyword.put(opts, :default_option, default_option))
  end

  @spec select_input(atom(), keyword()) :: select_input()
  def select_input(name, opts \\ []) do
    input(name, :select, opts)
  end

  @spec multiselect_input(atom(), keyword()) :: multiselect_input()
  def multiselect_input(name, opts \\ []) do
    input(name, :multiselect, opts)
  end

  @spec email_input(atom(), keyword()) :: email_input()
  def email_input(name, opts \\ []) do
    input(name, :email, opts)
  end

  @spec text_input(atom(), keyword()) :: text_input()
  def text_input(name, opts \\ []) do
    input(name, :text, opts)
  end

  defp input(name, type, opts) do
    default_label = name |> to_string() |> String.replace("_", " ") |> String.capitalize()
    default_label = if type == :boolean, do: default_label <> "?", else: default_label

    Enum.into(opts, %{
      name: name,
      type: type,
      label: Keyword.get(opts, :label, default_label)
    })
  end
end
