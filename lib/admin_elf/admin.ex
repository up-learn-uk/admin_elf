defmodule AdminElf.Admin do
  @type features_map() :: %{required(atom()) => module() | features_map()}

  @type menu_item() ::
          %{type: :dashboard}
          | %{type: :feature, feature: String.t()}
          | %{
              type: :group,
              title: String.t(),
              icon: String.t() | nil,
              features: [[atom()]]
            }

  @type menu_definition() :: [menu_item()]

  @callback title() :: String.t()
  @callback features(conn :: Plug.Conn.t()) :: features_map()
  @callback menu(conn :: Plug.Conn.t()) :: menu_definition()
end
