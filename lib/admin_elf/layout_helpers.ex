defmodule AdminElf.LayoutHelpers do
  @type component :: AdminElf.FieldHelpers.field() | AdminElf.InputHelpers.input() | panel()

  @type panel :: %{
          type: :panel,
          label: String.t(),
          children: [component()]
        }

  @spec panel(String.t(), [component()]) :: panel()
  def panel(label, children) do
    %{
      type: :panel,
      label: label,
      children: children
    }
  end

  @spec simple_layout([component()]) :: [panel()]
  def simple_layout(children) do
    [panel("Details", children)]
  end
end
