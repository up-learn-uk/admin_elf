defmodule AdminElf.Resource.Router do
  use Phoenix.Router

  scope "/", AdminElfWeb do
    resources "/:resource_id", ResourceController,
      only: [:new, :edit, :create, :update, :delete],
      param: "item_id"

    get "/:resource_id", ResourceController, :list
    get "/:resource_id/download", ResourceController, :download
    get "/:resource_id/:item_id", ResourceController, :detail
    post "/:resource_id/:item_id/custom-action/:action_name", ResourceController, :custom_action
    # POST /:resource_id/custom-action/:action
  end
end
