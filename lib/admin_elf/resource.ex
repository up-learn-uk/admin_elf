defmodule AdminElf.Resource do
  @behaviour AdminElf.FeatureType

  import AdminElf.Feature, only: [fetch_feature!: 3]
  alias AdminElf.Resource.Router

  defmacro __using__(_) do
    quote do
      import AdminElf.{ActionHelpers, FieldHelpers, InputHelpers, LayoutHelpers, TableHelpers}

      @behaviour AdminElf.Feature
      @behaviour AdminElf.Resource

      @impl AdminElf.Feature
      def type, do: unquote(__MODULE__)

      @impl AdminElf.Feature
      def title, do: plural_name()

      @impl AdminElf.Resource
      def plural_name, do: "#{name()}s"

      @impl AdminElf.Resource
      def item_id(item), do: item.id

      @impl AdminElf.Resource
      def item_display_name(item), do: item_id(item)

      @impl AdminElf.Resource
      def allow_creation(_conn), do: true

      @impl AdminElf.Resource
      def allow_download(_conn), do: false

      @impl AdminElf.Resource
      def actions(conn), do: []

      @impl AdminElf.Resource
      def quick_actions(_conn), do: [:detail, :edit, :delete]

      defoverridable title: 0,
                     plural_name: 0,
                     item_id: 1,
                     item_display_name: 1,
                     allow_creation: 1,
                     actions: 1,
                     quick_actions: 1,
                     allow_download: 1
    end
  end

  @impl AdminElf.FeatureType
  def router, do: Router

  @impl AdminElf.FeatureType
  def default_path(conn, resource_id) do
    Router.Helpers.resource_path(conn, :list, resource_id)
  end

  @callback name() :: String.t()
  @callback plural_name() :: String.t()

  @callback item_id(term) :: String.t()
  @callback item_display_name(term) :: String.t()

  @callback allow_creation(conn :: Plug.Conn.t()) :: boolean()
  @callback allow_download(conn :: Plug.Conn.t()) :: boolean()
  @callback actions(conn :: Plug.Conn.t()) :: [AdminElf.ActionHelpers.action()]
  @callback quick_actions(conn :: Plug.Conn.t()) :: [atom()] | keyword()
  @callback filters(conn :: Plug.Conn.t()) :: [AdminElf.InputHelpers.input()]
  @callback index_table(conn :: Plug.Conn.t()) :: [AdminElf.TableHelpers.column()]
  @callback details_layout(conn :: Plug.Conn.t()) :: [AdminElf.LayoutHelpers.component()]
  @callback form_layout(conn :: Plug.Conn.t()) ::
              [AdminElf.LayoutHelpers.component()] | {module(), binary(), keyword()}

  @callback list_resource(
              conn :: Plug.Conn.t(),
              page :: pos_integer(),
              per_page :: pos_integer(),
              order :: keyword(atom()),
              filter :: map()
            ) :: {items :: list(), count :: pos_integer()}
  @callback get_resource(
              conn :: Plug.Conn.t(),
              item_id :: String.t()
            ) :: {:ok, resource :: term()} | {:error, reason :: term()}
  @callback create_resource(
              conn :: Plug.Conn.t(),
              attrs :: any()
            ) :: {:ok, created_item :: any()} | {:error, reason :: term()}
  @callback update_resource(
              conn :: Plug.Conn.t(),
              item :: any(),
              attrs :: any()
            ) :: {:ok, updated_item :: any()} | {:error, reason :: term()}
  @callback delete_resource(
              conn :: Plug.Conn.t(),
              item :: term()
            ) :: {:ok, deleted_item :: any()} | {:error, reason :: term()}

  @optional_callbacks filters: 1,
                      index_table: 1,
                      form_layout: 1,
                      list_resource: 5,
                      create_resource: 2,
                      update_resource: 3,
                      delete_resource: 2

  def get_name(conn, admin, id) do
    fetch_feature!(conn, admin, id).name()
  end

  def get_plural_name(conn, admin, id) do
    fetch_feature!(conn, admin, id).plural_name()
  end

  def get_item_id(conn, admin, id, item) do
    fetch_feature!(conn, admin, id).item_id(item)
  end

  def get_item_display_name(conn, admin, id, item) do
    fetch_feature!(conn, admin, id).item_display_name(item)
  end

  def get_allow_creation(conn, admin, id) do
    fetch_feature!(conn, admin, id).allow_creation(conn)
  end

  def get_allow_download(conn, admin, id) do
    fetch_feature!(conn, admin, id).allow_download(conn)
  end

  def get_actions(conn, admin, id) do
    fetch_feature!(conn, admin, id).actions(conn)
  end

  def get_quick_actions(conn, admin, id) do
    fetch_feature!(conn, admin, id).quick_actions(conn)
  end

  def get_filters(conn, admin, id) do
    fetch_feature!(conn, admin, id).filters(conn)
  end

  def get_index_table(conn, admin, id) do
    fetch_feature!(conn, admin, id).index_table(conn)
  end

  def get_details_layout(conn, admin, id) do
    fetch_feature!(conn, admin, id).details_layout(conn)
  end

  def get_form_layout(conn, admin, id) do
    fetch_feature!(conn, admin, id).form_layout(conn)
  end

  def list_resource(conn, admin, id, page, per_page, order, filter) do
    fetch_feature!(conn, admin, id).list_resource(conn, page, per_page, order, filter)
  end

  def get_resource(conn, admin, id, item_id) do
    fetch_feature!(conn, admin, id).get_resource(conn, item_id)
  end

  def create_resource(conn, admin, id, params) do
    fetch_feature!(conn, admin, id).create_resource(conn, params)
  end

  def update_resource(conn, admin, id, item, attrs) do
    fetch_feature!(conn, admin, id).update_resource(conn, item, attrs)
  end

  def delete_resource(conn, admin, id, item) do
    fetch_feature!(conn, admin, id).delete_resource(conn, item)
  end
end
