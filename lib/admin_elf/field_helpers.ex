defmodule AdminElf.FieldHelpers do
  @type field_type :: :text | :date | :number | :boolean | :reference | :link

  @type field :: %{
          type: field_type(),
          name: atom(),
          label: String.t(),
          value: (term() -> term()),
          link: (term() -> term()) | nil,
          referenced_resource_id: any()
        }

  @spec text_field(atom(), keyword()) :: field()
  def text_field(name, opts \\ []) do
    field(name, :text, opts)
  end

  @spec date_field(atom(), keyword()) :: field()
  def date_field(name, opts \\ []) do
    value = fn item ->
      date = Map.fetch!(item, name)

      unless is_nil(date) do
        format = Keyword.get(opts, :format, get_default_date_format(date))

        Timex.format!(date, format)
      end
    end

    field(name, :date, Keyword.put_new(opts, :value, value))
  end

  @spec number_field(atom(), keyword()) :: field()
  def number_field(name, opts \\ []) do
    value = fn item ->
      number = Map.fetch!(item, name)

      case Keyword.get(opts, :format) do
        :currency ->
          Number.Currency.number_to_currency(number, Application.get_env(:admin_elf, :currency))

        :percentage ->
          number && Number.Percentage.number_to_percentage(number, precision: 2)

        _ ->
          Number.Delimit.number_to_delimited(number, precision: 0)
      end
    end

    field(name, :number, Keyword.put_new(opts, :value, value))
  end

  @spec boolean_field(atom(), keyword()) :: field()
  def boolean_field(name, opts \\ []) do
    field(name, :boolean, opts)
  end

  @spec reference_field(atom(), [atom()], keyword()) :: field()
  def reference_field(name, referenced_resource_id, opts \\ []) do
    opts = Keyword.put_new(opts, :referenced_resource_id, referenced_resource_id)

    field(name, :reference, opts)
  end

  @spec link_field(atom(), keyword()) :: field()
  def link_field(name, opts \\ []) do
    {text, opts} = Keyword.pop(opts, :text)

    field(name, :link, Keyword.put(opts, :value, fn _item -> text end))
  end

  @spec field(atom(), field_type(), keyword()) :: field()
  def field(name, type, opts \\ []) do
    default_label = name |> to_string() |> String.replace("_", " ") |> String.capitalize()
    default_label = if type == :boolean, do: default_label <> "?", else: default_label

    %{
      type: type,
      name: name,
      label: Keyword.get(opts, :label, default_label),
      value: Keyword.get(opts, :value, &Map.fetch!(&1, name)),
      link: Keyword.get(opts, :link),
      referenced_resource_id: Keyword.get(opts, :referenced_resource_id)
    }
  end

  defp get_default_date_format(%Date{}) do
    Application.get_env(:admin_elf, :date_format, "{YYYY}-{0M}-{0D}")
  end

  defp get_default_date_format(%module{}) when module in [DateTime, NaiveDateTime] do
    Application.get_env(:admin_elf, :datetime_format, "{YYYY}-{0M}-{0D} {h24}:{m}:{s}")
  end
end
