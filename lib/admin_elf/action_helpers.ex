defmodule AdminElf.ActionHelpers do
  @type action :: %{
          name: atom(),
          fun: fun(),
          label: String.t(),
          inputs: [AdminElf.InputHelpers.input()]
        }

  @spec action(atom(), fun(), keyword()) :: action()
  def action(name, fun, opts \\ []) do
    default_label = name |> to_string() |> String.replace("_", " ") |> String.capitalize()

    %{
      name: name,
      fun: fun,
      label: Keyword.get(opts, :label, default_label),
      inputs: Keyword.get(opts, :inputs)
    }
  end
end
