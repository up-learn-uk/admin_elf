defmodule AdminElf.Feature do
  @doc "Defines the feature title"
  @callback title() :: String.t()
  @doc "Defines the feature icon"
  @callback icon() :: String.t() | nil
  @doc "Defines the feature type"
  @callback type() :: module()

  def get_title(conn, admin, id), do: fetch_feature!(conn, admin, id).title()
  def get_icon(conn, admin, id), do: fetch_feature!(conn, admin, id).icon()
  def get_type(conn, admin, id), do: fetch_feature!(conn, admin, id).type()

  @spec fetch_feature!(conn :: Plug.Conn.t(), admin :: atom, id :: [atom]) :: module
  def fetch_feature!(conn, admin, id) do
    path = Enum.map(id, &Access.key!/1)

    conn
    |> admin.features()
    |> get_in(path)
  end
end
