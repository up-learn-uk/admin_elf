defmodule AdminElf.MenuHelpers do
  def dashboard do
    %{type: :dashboard}
  end

  def feature(id) do
    %{type: :feature, feature_id: id}
  end

  def group(title, opts) do
    %{
      type: :group,
      title: title,
      features: Keyword.fetch!(opts, :features),
      icon: Keyword.get(opts, :icon)
    }
  end
end
