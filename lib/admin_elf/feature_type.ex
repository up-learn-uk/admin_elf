defmodule AdminElf.FeatureType do
  @callback router() :: module
  @callback default_path(conn :: Plug.Conn.t(), feature_id :: String.t()) :: String.t()
end
