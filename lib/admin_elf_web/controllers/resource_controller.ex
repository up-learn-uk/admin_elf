defmodule AdminElfWeb.ResourceController do
  use AdminElfWeb, :controller

  alias AdminElf.Resource
  alias AdminElf.Resource.Router.Helpers, as: Routes
  alias AdminElfWeb.Helpers
  alias AdminElfWeb.ResourceView

  def list(conn, %{"resource_id" => resource_id} = params) do
    admin = Helpers.get_admin(conn)
    resource_id = Helpers.decode_id(resource_id)
    page = params |> Map.get("page", "1") |> String.to_integer()
    per_page = params |> Map.get("per_page", "25") |> String.to_integer()

    order = parse_order(params["order"])

    filter = params |> Map.get("filter", %{}) |> parse_filters(conn, admin, resource_id)

    {items, count} = list_resource(conn, admin, resource_id, page, per_page, order, filter)

    params = %{page: page, per_page: per_page, order: order, filter: filter}

    render(conn, "list.html", %{
      items: items,
      count: count,
      resource_id: resource_id,
      params: params
    })
  end

  def detail(conn, %{"resource_id" => resource_id, "item_id" => item_id}) do
    admin = Helpers.get_admin(conn)
    resource_id = Helpers.decode_id(resource_id)

    {:ok, item} = Resource.get_resource(conn, admin, resource_id, item_id)

    render(conn, "detail.html", %{resource_id: resource_id, item: item})
  end

  def new(conn, %{"resource_id" => resource_id}) do
    resource_id = Helpers.decode_id(resource_id)

    render(conn, "form.html", %{resource_id: resource_id})
  end

  def edit(conn, %{"resource_id" => resource_id, "item_id" => item_id}) do
    admin = Helpers.get_admin(conn)
    resource_id = Helpers.decode_id(resource_id)

    {:ok, item} = Resource.get_resource(conn, admin, resource_id, item_id)

    render(conn, "form.html", %{resource_id: resource_id, item: item})
  end

  def create(conn, %{"resource_id" => resource_id, "attrs" => attrs}) do
    admin = Helpers.get_admin(conn)
    id = Helpers.decode_id(resource_id)

    {message_type, message} =
      case Resource.create_resource(conn, admin, id, attrs) do
        {:ok, _created_resource} -> {:notice, "Resource created!"}
        {:error, _error} -> {:error, "An error occurred!"}
      end

    conn
    |> put_flash(message_type, message)
    |> redirect(to: Routes.resource_path(conn, :list, resource_id))
  end

  def update(conn, %{"resource_id" => resource_id, "item_id" => item_id, "attrs" => attrs}) do
    admin = Helpers.get_admin(conn)
    decoded_resource_id = Helpers.decode_id(resource_id)

    {message_type, message} =
      with {:ok, item} <- Resource.get_resource(conn, admin, decoded_resource_id, item_id),
           {:ok, _item} <- Resource.update_resource(conn, admin, decoded_resource_id, item, attrs) do
        {:notice, "Resource updated!"}
      else
        {:error, _item} -> {:error, "An error occurred!"}
      end

    conn
    |> put_flash(message_type, message)
    |> redirect(to: Resource.Router.Helpers.resource_path(conn, :edit, resource_id, item_id))
  end

  def delete(conn, %{"resource_id" => resource_id, "item_id" => item_id}) do
    admin = Helpers.get_admin(conn)
    decoded_resource_id = Helpers.decode_id(resource_id)

    {message_type, message} =
      with {:ok, resource} <- Resource.get_resource(conn, admin, decoded_resource_id, item_id),
           {:ok, _resource} <-
             Resource.delete_resource(conn, admin, decoded_resource_id, resource) do
        {:notice, "Resource deleted!"}
      else
        {:error, _resource} -> {:error, "An error occurred!"}
      end

    conn
    |> put_flash(message_type, message)
    |> redirect(to: Routes.resource_path(conn, :list, resource_id))
  end

  def download(conn, %{"resource_id" => resource_id} = params) do
    admin = Helpers.get_admin(conn)
    decoded_resource_id = Helpers.decode_id(resource_id)

    page = params |> Map.get("page", "1") |> String.to_integer()
    per_page = params |> Map.get("per_page", "25") |> String.to_integer()
    order = parse_order(params["order"])

    filter = params |> Map.get("filter", %{}) |> parse_filters(conn, admin, decoded_resource_id)

    fields = Resource.get_index_table(conn, admin, decoded_resource_id)

    headers = Enum.map(fields, & &1.label)

    {items, _count} =
      list_resource(conn, admin, decoded_resource_id, page, per_page, order, filter)

    items = convert_resource_by_index_table(items, fields, conn)

    data =
      items
      |> CSV.encode(headers: headers)
      |> Enum.to_list()
      |> to_string()

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=export-#{resource_id}.csv")
    |> put_root_layout(false)
    |> send_resp(200, data)
  end

  def custom_action(conn, params) do
    %{
      "resource_id" => resource_id,
      "item_id" => item_id,
      "action_name" => action_name
    } = params

    inputs = Map.get(params, "inputs")
    admin = Helpers.get_admin(conn)
    decoded_resource_id = Helpers.decode_id(resource_id)

    {:ok, item} = Resource.get_resource(conn, admin, decoded_resource_id, item_id)

    with {:ok, action} <- get_action(conn, resource_id, action_name),
         {:ok, message} <-
           action.fun.(conn, item, ensure_only_allowed_inputs(action.inputs, inputs)) do
      conn
      |> put_flash(:notice, message)
      |> redirect(to: Routes.resource_path(conn, :detail, resource_id, item_id))
    else
      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> redirect(to: Routes.resource_path(conn, :detail, resource_id, item_id))
    end
  end

  defp list_resource(conn, admin, resource_id, page, per_page, order, filter) do
    order =
      if order do
        [{order.direction, order.field}]
      else
        []
      end

    Resource.list_resource(conn, admin, resource_id, page, per_page, order, filter)
  end

  defp get_field_value(field, item, conn) do
    case {field.value.(item), field.type} do
      {nil, _} ->
        "--"

      {field_value, :reference} ->
        ResourceView.resource_item_display_name(
          conn,
          field.referenced_resource_id,
          field_value
        )

      {field_value, _} ->
        field_value
    end
  end

  defp convert_resource_by_index_table(items, fields, conn) do
    Enum.reduce(items, [], fn item, acc ->
      row =
        Enum.reduce(fields, %{}, fn field, acc ->
          Map.put(acc, field.label, get_field_value(field, item, conn))
        end)

      acc ++ [row]
    end)
  end

  defp parse_filters(raw_filters, conn, admin, resource_id) do
    conn
    |> Resource.get_filters(admin, resource_id)
    |> Enum.reduce(%{}, fn filter_definition, filters ->
      value = Map.get(raw_filters, to_string(filter_definition.name), "")

      cond do
        filter_definition.type in ~w[text number date select checkboxes]a and value != "" ->
          Map.put(filters, filter_definition.name, value)

        filter_definition.type == :radio and value == "" ->
          Map.put(filters, filter_definition.name, filter_definition.default_value)

        filter_definition.type == :radio ->
          Map.put(filters, filter_definition.name, value)

        filter_definition.type == :boolean and value in ~w[true false] ->
          Map.put(filters, filter_definition.name, value == "true")

        true ->
          filters
      end
    end)
  end

  defp get_action(conn, resource_id, action_name) do
    admin = Helpers.get_admin(conn)
    decoded_resource_id = Helpers.decode_id(resource_id)

    conn
    |> Resource.get_actions(admin, decoded_resource_id)
    |> Enum.find(&(to_string(&1.name) == action_name))
    |> case do
      nil -> {:error, :invalid_action}
      action -> {:ok, action}
    end
  end

  defp ensure_only_allowed_inputs(nil, _inputs_sent), do: nil

  defp ensure_only_allowed_inputs(allowed_inputs, inputs_sent) do
    Map.take(inputs_sent, Enum.map(allowed_inputs, &to_string(&1.name)))
  end

  defp parse_order(order) do
    case order do
      %{"field" => field, "direction" => direction} when direction in ~w[asc desc] ->
        %{field: String.to_existing_atom(field), direction: String.to_existing_atom(direction)}

      %{"field" => field} ->
        %{field: String.to_existing_atom(field), direction: :asc}

      _ ->
        nil
    end
  end
end
