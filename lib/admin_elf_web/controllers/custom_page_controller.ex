defmodule AdminElfWeb.CustomPageController do
  use AdminElfWeb, :controller

  alias AdminElf.CustomPage
  alias AdminElfWeb.Helpers

  def index(conn, %{"custom_page_id" => custom_page_id} = params) do
    admin = Helpers.get_admin(conn)
    custom_page_id = Helpers.decode_id(custom_page_id)

    {view, template, assigns} = CustomPage.get_template(conn, admin, custom_page_id, params)

    if AdminElfWeb.ViewHelpers.live_view?(view) do
      Phoenix.LiveView.Controller.live_render(conn, view, assigns)
    else
      conn
      |> put_view(view)
      |> render(template, assigns)
    end
  end
end
