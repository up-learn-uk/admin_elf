defmodule AdminElfWeb.PageController do
  use AdminElfWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
