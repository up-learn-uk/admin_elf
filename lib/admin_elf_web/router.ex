defmodule AdminElfWeb.Router do
  use AdminElfWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery

    plug :put_secure_browser_headers, %{
      "content-security-policy" =>
        Application.compile_env(
          :admin_elf,
          :content_security_policy,
          "default-src 'self'; script-src 'self'; img-src * data: blob: 'self'"
        )
    }
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AdminElfWeb do
    pipe_through :browser

    get "/", PageController, :index

    forward "/", FeatureRouterDispatcher
  end

  # Other scopes may use custom stacks.
  # scope "/api", AdminElfWeb do
  #   pipe_through :api
  # end
end
