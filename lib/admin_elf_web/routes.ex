defmodule AdminElfWeb.Routes do
  def static_path(_conn, path), do: Path.join("/admin-elf", path)
end
