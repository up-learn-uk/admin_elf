defmodule AdminElfWeb.ResourceView do
  use AdminElfWeb, :view

  alias AdminElf.Resource
  alias AdminElf.Resource.Router.Helpers, as: Routes
  alias AdminElfWeb.Helpers

  def resource_name(conn, resource_id) do
    Resource.get_name(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_item_id(conn, feature_id, item) do
    admin = Helpers.get_admin(conn)

    conn
    |> Resource.get_item_id(admin, feature_id, item)
    |> to_string()
  end

  def resource_item_display_name(conn, feature_id, item) do
    admin = Helpers.get_admin(conn)

    Resource.get_item_display_name(conn, admin, feature_id, item)
  end

  def resource_allow_creation(conn, resource_id) do
    Resource.get_allow_creation(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_allow_download(conn, resource_id) do
    Resource.get_allow_download(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_actions(conn, resource_id) do
    Resource.get_actions(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_quick_actions(conn, resource_id) do
    conn
    |> Resource.get_quick_actions(Helpers.get_admin(conn), resource_id)
    |> Enum.map(fn
      {action, opts} -> {action, opts}
      action -> {action, []}
    end)
  end

  def resource_filters(conn, resource_id) do
    Resource.get_filters(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_index_table(conn, resource_id) do
    Resource.get_index_table(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_details_layout(conn, resource_id) do
    Resource.get_details_layout(conn, Helpers.get_admin(conn), resource_id)
  end

  def resource_form_layout(conn, resource_id) do
    Resource.get_form_layout(conn, Helpers.get_admin(conn), resource_id)
  end

  def update_resource_order(conn, resource_id, %{order: order} = params, order_field) do
    new_params =
      cond do
        not is_nil(order) and order.field == order_field and order.direction == :asc ->
          Map.put(params, :order, field: order_field, direction: :desc)

        not is_nil(order) and order.field == order_field and order.direction == :desc ->
          Map.put(params, :order, nil)

        true ->
          Map.put(params, :order, field: order_field, direction: :asc)
      end

    Routes.resource_path(conn, :list, Helpers.encode_id(resource_id), new_params)
  end
end
