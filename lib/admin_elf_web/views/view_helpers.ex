defmodule AdminElfWeb.ViewHelpers do
  use PhoenixHTMLHelpers

  import Phoenix.View

  alias AdminElf.Feature
  alias AdminElfWeb.Helpers
  alias AdminElfWeb.PartialView
  alias AdminElfWeb.Routes

  def capitalize(string) do
    String.capitalize(string)
  end

  def feather_icon(conn, icon, opts \\ []) do
    default_attrs = [
      width: "24",
      height: "24",
      fill: "none",
      stroke: "currentColor",
      stroke_width: "2",
      stroke_linecap: "round",
      stroke_linejoin: "round"
    ]

    {size, opts} = Keyword.pop(opts, :size)

    opts =
      if size do
        default_attrs
        |> Keyword.merge(opts)
        |> Keyword.merge(width: size, height: size)
      else
        Keyword.merge(default_attrs, opts)
      end

    content_tag(:svg, opts) do
      tag(:use, "xlink:href": Routes.static_path(conn, "/icons/feather-sprite.svg\##{icon}"))
    end
  end

  def feature_icon(conn, feature_id, opts \\ []) do
    icon = Feature.get_icon(conn, Helpers.get_admin(conn), feature_id)

    if not is_nil(icon), do: feather_icon(conn, icon, opts)
  end

  def feature_title(conn, feature_id) do
    Feature.get_title(conn, Helpers.get_admin(conn), feature_id)
  end

  def title(conn) do
    Helpers.get_admin(conn).title()
  end

  def menu(conn) do
    Helpers.get_admin(conn).menu(conn)
  end

  def feature_default_path(conn, feature_id) do
    admin = Helpers.get_admin(conn)
    feature = Feature.get_type(conn, admin, feature_id)

    feature.default_path(conn, Helpers.encode_id(feature_id))
  end

  def feature_menu_active?(conn, feature_id) do
    default_path = feature_default_path(conn, feature_id)

    conn.request_path == default_path or
      String.starts_with?(conn.request_path, default_path <> "/")
  end

  def render_partial(view \\ PartialView, partial, opts) do
    conn = Keyword.get(opts, :conn)

    if not is_nil(conn) and AdminElfWeb.ViewHelpers.live_view?(view) do
      Phoenix.LiveView.Controller.live_render(conn, view, opts)
    else
      render(view, partial, opts)
    end
  end

  def live_view?(view), do: function_exported?(view, :__live__, 0)
end
