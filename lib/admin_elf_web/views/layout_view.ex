defmodule AdminElfWeb.LayoutView do
  use AdminElfWeb, :view

  def live_socket_path, do: Application.get_env(:admin_elf, :live_socket_path)

  def additional_css_paths, do: Application.get_env(:admin_elf, :additional_css_paths, [])
  def additional_js_paths, do: Application.get_env(:admin_elf, :additional_js_paths, [])
end
