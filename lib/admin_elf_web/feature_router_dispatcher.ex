defmodule AdminElfWeb.FeatureRouterDispatcher do
  @behaviour Plug

  alias AdminElf.Feature
  alias AdminElfWeb.Helpers

  @impl true
  def init(opts), do: opts

  @impl true
  def call(%Plug.Conn{path_info: [feature_id | _]} = conn, _opts) do
    admin = Helpers.get_admin(conn)
    id = Helpers.decode_id(feature_id)

    feature_type = Feature.get_type(conn, admin, id)

    feature_type.router().call(conn, feature_type.router().init([]))
  end
end
