defmodule AdminElfWeb.Helpers do
  @spec get_admin(Plug.Conn.t()) :: module
  def get_admin(conn) do
    conn.private
    |> Map.fetch!(AdminElf)
    |> Map.fetch!(:admin)
  end

  @spec decode_id(String.t()) :: [atom]
  def decode_id(id) do
    id
    |> String.split("__")
    |> Enum.map(&String.to_existing_atom/1)
  end

  @spec encode_id([term]) :: String.t()
  def encode_id(id), do: Enum.join(id, "__")
end
