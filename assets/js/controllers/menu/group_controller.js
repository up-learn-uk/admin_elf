import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["group", "openIcon", "closeIcon", "groupContent"];
  static classes = ["active", "inactive"];
  static values = { active: Boolean };

  activeValueChanged() {
    this.openIconTarget.hidden = this.activeValue;
    this.closeIconTarget.hidden = !this.activeValue;
    this.groupContentTarget.hidden = !this.activeValue;

    if (this.activeValue) {
      this.groupTarget.classList.add(this.activeClass);
      this.groupTarget.classList.remove(this.inactiveClass);
    } else {
      this.groupTarget.classList.remove(this.activeClass);
      this.groupTarget.classList.add(this.inactiveClass);
    }
  }

  toggleMenuGroupActive() {
    this.activeValue = !this.activeValue;
  }
}
