import { screen } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";
import userEvent from "@testing-library/user-event";

import GroupController from "./group_controller";

beforeEach(() => {
  setupControllers({ group: GroupController });
});

afterEach(() => {
  cleanUpDOM();
});

describe("menu controller", () => {
  test("it displays or hides the menu content and icon when the menu is toggled", async () => {
    await mountHTML(`
      <div
        data-testid="container"
        data-controller="group"
        data-group-active-value="false"
        data-group-active-class="active-class"
        data-group-inactive-class="inactive-class"
        data-group-target="group"
      >
        <button data-action="click->group#toggleMenuGroupActive">Toggle</button>
        <div data-group-target="openIcon">Open icon</div>
        <div data-group-target="closeIcon">Close icon</div>
        <div data-group-target="groupContent">Group content</div>
      </div>
    `);

    expect(screen.getByText("Group content")).not.toBeVisible();
    expect(screen.getByText("Open icon")).toBeVisible();
    expect(screen.getByText("Close icon")).not.toBeVisible();
    expect(screen.getByTestId("container")).toHaveClass("inactive-class");
    expect(screen.getByTestId("container")).not.toHaveClass("active-class");

    await userEvent.click(screen.getByText("Toggle"));

    expect(screen.getByText("Group content")).toBeVisible();
    expect(screen.getByText("Open icon")).not.toBeVisible();
    expect(screen.getByText("Close icon")).toBeVisible();
    expect(screen.getByTestId("container")).toHaveClass("active-class");
    expect(screen.getByTestId("container")).not.toHaveClass("inactive-class");
  });
});
