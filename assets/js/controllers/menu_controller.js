import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["menuIcon", "menuContent"];
  static classes = ["open", "closed"];
  static values = { open: Boolean };

  openValueChanged() {
    this.element.classList.add(
      this.openValue ? this.openClass : this.closedClass
    );
    this.element.classList.remove(
      this.openValue ? this.closedClass : this.openClass
    );

    this.menuIconTarget.hidden = this.openValue;
    this.menuContentTarget.hidden = !this.openValue;
  }

  toggleIsMenuOpen() {
    this.openValue = !this.openValue;
  }
}
