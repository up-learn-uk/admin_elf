import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["radio", "clearValue"];
  static values = { empty: Boolean };

  initialize() {
    this.emptyValue = true;
    this.setNotEmpty = this.setNotEmpty.bind(this);
    this.setEmpty = this.setEmpty.bind(this);
  }

  connect() {
    this.radioTargets.forEach((radio) => {
      radio.addEventListener("change", this.setNotEmpty);
    });

    this.clearValueTarget.addEventListener("click", this.setEmpty);

    if (this.radioTargets.some((r) => r.checked)) {
      this.setNotEmpty();
    }
  }

  disconnect() {
    this.radioTargets.forEach((radio) => {
      radio.removeEventListener("change", this.setNotEmpty);
    });

    this.clearValueTarget.removeEventListener("click", this.setEmpty);
  }

  emptyValueChanged() {
    this.clearValueTarget.hidden = this.emptyValue === true;
  }

  setNotEmpty() {
    this.emptyValue = false;
  }

  setEmpty() {
    this.radioTargets.forEach((radio) => (radio.checked = false));
    this.emptyValue = true;
  }
}
