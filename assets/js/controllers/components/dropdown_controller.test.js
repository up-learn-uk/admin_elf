import { screen, waitFor } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";
import userEvent from "@testing-library/user-event";

import DropdownController from "./dropdown_controller";

beforeEach(() => {
  setupControllers({ dropdown: DropdownController });
});

afterEach(() => {
  cleanUpDOM();
});

describe("dropdown controller", () => {
  it("shows/hides the dropdown content when the button is clicked", async () => {
    await mountHTML(`
      <div class="relative"
          data-controller="dropdown"
          data-action="click->dropdown#toggle click@window->dropdown#hide"
          data-dropdown-active-target="#dropdown-button"
          data-dropdown-active-class="bg-teal-600"
          data-dropdown-invisible-class="opacity-0 scale-95"
          data-dropdown-visible-class="opacity-100 scale-100"
          data-dropdown-entering-class="ease-out duration-100"
          data-dropdown-enter-timeout="100"
          data-dropdown-leaving-class="ease-in duration-75"
          data-dropdown-leave-timeout="75">
        <div data-action="click->dropdown#toggle click@window->dropdown#hide" role="button" data-dropdown-target="button" tabindex="0" class="inline-block select-none">
          Open Dropdown
        </div>
        <div data-testid="dropdown-menu" data-dropdown-target="menu" class="absolute pin-r mt-2 transform transition hidden opacity-0 scale-95">
          <div class="bg-white shadow rounded border overflow-hidden">
            Content
          </div>
        </div>
      </div>
    `);

    expect(screen.getByTestId("dropdown-menu")).toHaveClass("hidden");

    userEvent.click(screen.queryByRole("button", { name: "Open Dropdown" }));

    waitFor(() => {
      expect(screen.getByTestId("dropdown-menu")).not.toHaveClass("hidden");
    });

    userEvent.click(screen.queryByRole("button", { name: "Open Dropdown" }));

    expect(screen.getByTestId("dropdown-menu")).toHaveClass("hidden");
  });
});
