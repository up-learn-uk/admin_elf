import { screen, waitFor } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";
import userEvent from "@testing-library/user-event";

import ModalController from "./modal_controller";

beforeEach(() => {
  setupControllers({ modal: ModalController });
});

afterEach(() => {
  cleanUpDOM();
});

const modalHTML = `
  <div data-controller="modal" data-modal-allow-background-close="false">
    <button data-action="click->modal#open" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-3 px-6 rounded">
      <span>Open Modal</span>
    </button>

    <div data-testid="modal-container" data-modal-target="container" data-action="click->modal#closeBackground keyup@window->modal#closeWithKeyboard" class="hidden animated fadeIn fixed inset-0 overflow-y-auto flex items-center justify-center" style="z-index: 9999;">
      <div class="max-h-screen w-full max-w-lg relative">
        <div class="m-1 bg-white rounded shadow">
          <div class="p-8">
            <h2 class="text-xl mb-4">Modal Content</h2>
            <p class="mb-4">This is an example modal dialog box.</p>

            <div class="flex justify-end items-center flex-wrap mt-6">
              <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" data-action="click->modal#close">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
`;

describe("modal controller", () => {
  it("shows the modal container when the button is clicked", async () => {
    await mountHTML(modalHTML);

    expect(screen.getByTestId("modal-container")).toHaveClass("hidden");

    await userEvent.click(screen.queryByRole("button", { name: "Open Modal" }));

    expect(screen.getByTestId("modal-container")).not.toHaveClass("hidden");
  });

  it("hides the modal container when the close button is clicked", async () => {
    await mountHTML(modalHTML);

    expect(screen.getByTestId("modal-container")).toHaveClass("hidden");

    await userEvent.click(screen.queryByRole("button", { name: "Open Modal" }));

    expect(screen.getByTestId("modal-container")).not.toHaveClass("hidden");

    await userEvent.click(screen.queryByRole("button", { name: "Close" }));

    expect(screen.getByTestId("modal-container")).toHaveClass("hidden");
  });
});
