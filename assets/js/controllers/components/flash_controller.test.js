import { screen, waitFor } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";
import userEvent from "@testing-library/user-event";

import FlashController from "./flash_controller";

beforeEach(() => {
  setupControllers({ "flash": FlashController });
});

afterEach(() => {
  cleanUpDOM();
});

describe("flash controller", () => {
  it("allows removing the flash message from the screen", async () => {
    await mountHTML(`
      <div data-controller="flash" data-flash-target="container" data-testid="flash-message">
        <p>Message here</p>
        <button data-action="click->flash#remove" class="cursor-pointer">Remove</button>
      </div>
    `);

    expect(screen.queryByRole("button", { name: "Remove" })).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Remove" }));

    expect(screen.queryByTestId("flash-message")).not.toBeInTheDocument();
  });
});
