import { screen, waitFor } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";
import userEvent from "@testing-library/user-event";

import BooleanInputController from "./boolean_input_controller";

beforeEach(() => {
  setupControllers({ "boolean-input": BooleanInputController });
});

afterEach(() => {
  cleanUpDOM();
});

describe("boolean input controller", () => {
  it("does not show the clear button unless one of the radios are checked", async () => {
    await mountHTML(`
      <div data-controller="boolean-input">
        <button data-boolean-input-target="clearValue">Clear</button>
        <label>
          <input type="radio" data-boolean-input-target="radio" /> Yes
        </label>
        <label>
          <input type="radio" data-boolean-input-target="radio" /> No
        </label>
      </div>
    `);

    expect(
      screen.queryByRole("button", { name: "Clear" })
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByLabelText("Yes"));

    expect(screen.getByRole("button", { name: "Clear" })).toBeInTheDocument();
  });

  it("shows the clear button if any radio starts checked", async () => {
    await mountHTML(`
      <div data-controller="boolean-input">
        <button data-boolean-input-target="clearValue">Clear</button>
        <label>
          <input type="radio" data-boolean-input-target="radio" checked /> Yes
        </label>
        <label>
          <input type="radio" data-boolean-input-target="radio" /> No
        </label>
      </div>
    `);

    await waitFor(() =>
      expect(screen.getByRole("button", { name: "Clear" })).toBeInTheDocument()
    );
  });

  it("clear the radio buttons when the clear button is pressed", async () => {
    await mountHTML(`
      <div data-controller="boolean-input">
        <button data-boolean-input-target="clearValue">Clear</button>
        <label>
          <input type="radio" data-boolean-input-target="radio" checked /> Yes
        </label>
        <label>
          <input type="radio" data-boolean-input-target="radio" /> No
        </label>
      </div>
    `);

    await waitFor(() =>
      expect(screen.getByRole("button", { name: "Clear" })).toBeInTheDocument()
    );

    await userEvent.click(screen.getByRole("button", { name: "Clear" }));

    screen.getAllByRole("radio").forEach((radio) => {
      expect(radio).not.toBeChecked();
    });

    expect(
      screen.queryByRole("button", { name: "Clear" })
    ).not.toBeInTheDocument();
  });
});
