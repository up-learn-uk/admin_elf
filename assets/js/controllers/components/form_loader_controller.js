import { Controller } from "@hotwired/stimulus";

export default class FormLoaderController extends Controller {
  connect() {
    this.form = this.element.closest("form")
  }

  handleSubmit(event) {
    event.preventDefault();
    this.element.classList.add("disabled");
    this.showLoader();
    this.form.submit();
  }

  showLoader() {
    const fullPageLoader = document.getElementById("full-page-loader");
    fullPageLoader.classList.remove("hidden")
  }
}
