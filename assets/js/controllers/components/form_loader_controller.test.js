import { screen } from "@testing-library/dom";
import userEvent from "@testing-library/user-event";

import FormLoaderController from "./form_loader_controller";
import { setupControllers, cleanUpDOM, mountHTML } from "../../testHelpers";

beforeEach(() => {
  setupControllers({ "form-loader": FormLoaderController });
  HTMLFormElement.prototype.submit = jest.fn();
});

afterEach(() => {
  cleanUpDOM();
});

describe("form-loader controller", () => {
  it("tests loader - before and after form submit", async () => {
    await mountHTML(`
      <div data-testid="full-page-loader" id="full-page-loader" class="fixed inset-0 flex items-center justify-center bg-gray-200 opacity-50 z-50 hidden">
        <div class="loader-container">
          <div class="w-10 h-10 border-t-4 border-b-4 border-gray-800 rounded-full animate-spin"></div>
        </div>
      </div>

      <form>
        <div data-controller="form-loader" class="flex justify-end items-center flex-wrap mt-6 gap-2">
          <button data-testid="submit" type="submit" data-action="click->form-loader#handleSubmit">Submit</button>
        </div>
      </form>
    `);

    expect(screen.getByTestId('full-page-loader')).toHaveClass("hidden");

    await userEvent.click(screen.getByTestId("submit"));

    expect(screen.getByTestId('full-page-loader')).not.toHaveClass("hidden");
  });
});
