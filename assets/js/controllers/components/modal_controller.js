import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["container"];
  static values = {
    backdropColor: { type: String, default: "rgba(0, 0, 0, 0.8)" },
    restoreScroll: { type: Boolean, default: true },
  };

  connect() {
    this.toggleClass = this.data.get("class") || "hidden";

    this.backgroundId = this.data.get("backgroundId") || "modal-background";

    this.backgroundHtml =
      this.data.get("backgroundHtml") || this._backgroundHTML();

    this.allowBackgroundClose =
      (this.data.get("allowBackgroundClose") || "true") === "true";

    this.preventDefaultActionOpening =
      (this.data.get("preventDefaultActionOpening") || "true") === "true";

    this.preventDefaultActionClosing =
      (this.data.get("preventDefaultActionClosing") || "true") === "true";
  }

  disconnect() {
    this.close();
  }

  open(e) {
    if (this.preventDefaultActionOpening) {
      e.preventDefault();
    }

    if (e.target.blur) {
      e.target.blur();
    }

    this.lockScroll();
    this.unhideModal();
    this.insertBackground();
  }

  close(e) {
    if (e && this.preventDefaultActionClosing) {
      e.preventDefault();
    }

    this.unlockScroll();
    this.hideModal();
    this.removeBackground();
  }

  closeBackground(e) {
    if (this.allowBackgroundClose && e.target === this.containerTarget) {
      this.close(e);
    }
  }

  closeWithKeyboard(e) {
    if (
      e.keyCode === 27 &&
      !this.containerTarget.classList.contains(this.toggleClass)
    ) {
      this.close(e);
    }
  }

  _backgroundHTML() {
    return `<div id="${this.backgroundId}" class="fixed top-0 left-0 w-full h-full" style="background-color: ${this.backdropColorValue}; z-index: 9998;"></div>`;
  }

  lockScroll() {
    // Add right padding to the body so the page doesn't shift
    // when we disable scrolling
    const scrollbarWidth =
      window.innerWidth - document.documentElement.clientWidth;
    document.body.style.paddingRight = `${scrollbarWidth}px`;

    this.saveScrollPosition();

    // Add classes to body to fix its position
    document.body.classList.add("fixed", "inset-x-0", "overflow-hidden");

    // Add negative top position in order for body to stay in place
    document.body.style.top = `-${this.scrollPosition}px`;
  }

  unlockScroll() {
    // Remove tweaks for scrollbar
    document.body.style.paddingRight = null;

    // Remove classes from body to unfix position
    document.body.classList.remove("fixed", "inset-x-0", "overflow-hidden");

    if (this.restoreScrollValue) {
      this.restoreScrollPosition();
    }

    // Remove the negative top inline style from body
    document.body.style.top = null;
  }

  saveScrollPosition() {
    this.scrollPosition = window.pageYOffset || document.body.scrollTop;
  }

  restoreScrollPosition() {
    if (this.scrollPosition === undefined) return;

    document.documentElement.scrollTop = this.scrollPosition;
  }

  hideModal() {
    this.containerTarget.classList.add(this.toggleClass);
  }

  unhideModal() {
    this.containerTarget.classList.remove(this.toggleClass);
  }

  insertBackground() {
    if (!this.data.get("disable-backdrop")) {
      document.body.insertAdjacentHTML("beforeend", this.backgroundHtml);
      this.background = document.querySelector(`#${this.backgroundId}`);
    }
  }

  removeBackground() {
    if (this.background) {
      this.background.remove();
    }
  }
}
