import { screen } from "@testing-library/dom";
import { setupControllers, cleanUpDOM, mountHTML } from "../testHelpers";
import userEvent from "@testing-library/user-event";

import MenuController from "./menu_controller";

beforeEach(() => {
  setupControllers({ menu: MenuController });
});

afterEach(() => {
  cleanUpDOM();
});

describe("menu controller", () => {
  test("it displays or hides the menu content and icon when the menu is toggled", async () => {
    await mountHTML(`
      <div
        data-testid="container"
        data-controller="menu"
        data-menu-open-value="true"
        data-menu-open-class="open-class"
        data-menu-closed-class="closed-class"
      >
        <button data-action="click->menu#toggleIsMenuOpen">Toggle</button>
        <div data-menu-target="menuIcon">Menu icon</div>
        <div data-menu-target="menuContent">Menu content</div>
      </div>
    `);

    expect(screen.getByText("Menu content")).toBeVisible();
    expect(screen.getByText("Menu icon")).not.toBeVisible();
    expect(screen.getByTestId("container")).toHaveClass("open-class");
    expect(screen.getByTestId("container")).not.toHaveClass("closed-class");

    await userEvent.click(screen.getByText("Toggle"));

    expect(screen.getByText("Menu content")).not.toBeVisible();
    expect(screen.getByText("Menu icon")).toBeVisible();
    expect(screen.getByTestId("container")).toHaveClass("closed-class");
    expect(screen.getByTestId("container")).not.toHaveClass("open-class");
  });
});
