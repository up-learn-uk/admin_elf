import { Application } from "@hotwired/stimulus";
import { within } from "@testing-library/dom";

export async function mountHTML(html) {
  const container = document.createElement("div");

  container.innerHTML = html;
  document.body.appendChild(container);

  return {
    container,
    ...within(container),
  };
}

export function cleanUpDOM() {
  document.body.innerHTML = "";
}

export function setupControllers(controllers) {
  const app = Application.start();

  Object.entries(controllers).forEach(([name, controller]) => {
    app.register(name, controller);
  });
}
