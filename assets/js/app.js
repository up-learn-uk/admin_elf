import "../css/app.scss";

import "phoenix_html";

import { Application } from "@hotwired/stimulus";
import { definitionsFromContext } from "@hotwired/stimulus-webpack-helpers";
import { Socket } from "phoenix";
import { LiveSocket } from "phoenix_live_view";
import SlimSelect from 'slim-select'

const Stimulus = Application.start();
const context = require.context("./controllers", true, /\_controller.js$/);
const definitions = definitionsFromContext(context);

Stimulus.load(definitions);

const socketPath = document
  .querySelector("html")
  .getAttribute("phx-socket");

const csrfToken = document
  .querySelector("meta[name='csrf-token']")
  .getAttribute("content");

const liveSocket = new LiveSocket(socketPath || "/live", Socket, {
  params: { _csrf_token: csrfToken },
});

liveSocket.connect();

window.liveSocket = liveSocket;

document.addEventListener("DOMContentLoaded", () => {
  document.querySelectorAll('select[multiple]').forEach((el, _) => {
    const options = JSON.parse(el.getAttribute('data-options'));
    const selected = JSON.parse(el.getAttribute('data-selected'));
    new SlimSelect({
      select: '#' + el.getAttribute('id'),
      data: options.map((option) => ({...option, selected: selected.includes(option.value)})),
    })
  });
});
