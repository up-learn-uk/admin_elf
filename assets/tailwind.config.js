const colors = require("tailwindcss/colors");

module.exports = {
  content: ["../lib/**/*.ex", "../lib/**/*.heex", "./js/**/*.js"],
  theme: {
    extend: {
      colors: {
        purple: colors.violet,
      },
      transitionProperty: {
        width: "width",
      },
      backgroundImage: {
        'floating-cogs': "url('../static/images/floating-cogs.svg')",
      }
    }
  },
  plugins: [
    require("@tailwindcss/forms"),
    // ...
  ],
};
