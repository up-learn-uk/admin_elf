# Authentication
We believe that authentication and authorization lie outside the remit of AdmiElf. Doing so allows maximum flexibility for the library to be used in a variety of settings and implementations. If you are setting up AdminElf in your application, this guide will provide an inspiration of how authentication can be integrated in your app. We provide more concrete examples of using basic auth and JWT based authentication, but the key principle remains the same: Use your `Router` module to carry out authentication before the request reaches AdminElf.

## Basic Auth
One way to ensure the security of your admin page is using basic auth.

> Full documentation can be found [here](https://hexdocs.pm/plug/Plug.BasicAuth.html)

In the `router.ex` file, configure:

```elixir
defmodule MyAppWeb.Router do
  # ...

  import Plug.BasicAuth

  pipeline :admin do
    plug :basic_auth,
      username: "hello",
      password: "secret"
  end

  scope "/admin" do
    pipe_through :admin

    forward "/", AdminElf.Plug, admin: MyAppWeb.Admin
  end

  # ...
end
```

With the above example, when the user goes to the path `/admin` they will be need to provide the user name and password configured in the `:basic_auth` plug.

## Token based authentication
Token based authentication can be implemented using the [`guardian`](https://hex.pm/packages/guardian) dependency.

> Full documentation can be found [here](https://hexdocs.pm/guardian/tutorial-start.html)

The code sample below ensures that our `/admin` route is protected and the user is authenticated using JWT:

```elixir
defmodule MyAppWeb.Router do
  # ...

  pipeline :auth do
    plug MyApp.UserManager.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  # ...

  scope "/admin" do
    pipe_through [:browser, :auth, :ensure_auth]

    forward "/", AdminElf.Plug, admin: MyAppWeb.Admin
  end

  # ...
end
```
